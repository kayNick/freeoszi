#ifndef GENVIEW_H
#define GENVIEW_H

#include <QChart>
#include <QChartView>
#include <QLineSeries>
#include <QValueAxis>
#include <QPointF>
#include <QtMath>


QT_CHARTS_BEGIN_NAMESPACE
class QLineSeries;
class QChart;
QT_CHARTS_END_NAMESPACE
QT_CHARTS_USE_NAMESPACE


class genView : public QObject
{
    Q_OBJECT

public:
    genView();
    QChartView * getChartView();
    void initSeries(unsigned char *, int length=512); // get series in constructor
    void updateSeries(unsigned char *, int length=512); // update series on click
private:
    QChart *m_chart = nullptr;
    QChartView *tView = nullptr;
    QLineSeries *sigSeries = nullptr;
    QValueAxis *sigAxis = nullptr, *timeAxis = nullptr;
    QVector<QPointF> *sigVector = nullptr;
};

#endif // GENVIEW_H
