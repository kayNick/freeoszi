
#include <QSlider>
#include <QSpinBox>
#include <QLabel>
#include <QtMath>


#include "deviceHandler.h"
#include "genWin.h"


genWin::genWin(deviceHandler *dH,  QWidget *parent) : devHandle(dH), genOp(new genOperator(dH))
    {
    this->setWindowTitle("RF-Generator");
    QGridLayout *gridLayout = new QGridLayout(this);
    // #####################################################  frequency ###############
    QLabel *freqTLabel = new QLabel("Frequency:",this);
    QLabel *freqLabel = new QLabel("30.1508 kHz",this);
    gridLayout->addWidget(freqTLabel, 0, 0, 1, 1);
    gridLayout->addWidget(freqLabel, 1, 0, 1, 1);

    QLabel *freq2MHzTxt = new QLabel("10 kHz", this);
    QSlider *freqSl2MHz = new QSlider(this);
    freqSl2MHz->setObjectName("slider2MHz");
    freqSl2MHz->setMaximum(199);
    freqSl2MHz->setMinimum(0);
    freqSl2MHz->setOrientation(Qt::Vertical);
    freqSl2MHz->setTickPosition(QSlider::TicksAbove);
    freqSl2MHz->setTickInterval(50);
    freqSl2MHz->setSingleStep(1);
    freqSl2MHz->setPageStep(10);
    gridLayout->addWidget(freq2MHzTxt, 0, 1, 1, 1);
    gridLayout->addWidget(freqSl2MHz, 1, 1, 1, 1);

    QLabel *freq10kHzTxt = new QLabel("50 Hz", this);
    QSlider *freqSl10kHz = new QSlider(this);
    freqSl10kHz->setObjectName("slider10kHz");
    freqSl10kHz->setMaximum(199);
    freqSl10kHz->setMinimum(0);
    freqSl10kHz->setOrientation(Qt::Vertical);
    freqSl10kHz->setTickPosition(QSlider::TicksAbove);
    freqSl10kHz->setTickInterval(50);
    freqSl10kHz->setSingleStep(1);
    freqSl10kHz->setPageStep(10);
    gridLayout->addWidget(freq10kHzTxt, 0, 2, 1, 1);
    gridLayout->addWidget(freqSl10kHz, 1, 2, 1, 1);

    QLabel *freq50HzTxt = new QLabel("0.25 Hz", this);
    QSlider *freqSl50Hz = new QSlider(this);
    freqSl50Hz->setObjectName("slider50Hz");
    freqSl50Hz->setMaximum(199);
    freqSl50Hz->setMinimum(0);
    freqSl50Hz->setOrientation(Qt::Vertical);
    freqSl50Hz->setTickPosition(QSlider::TicksAbove);
    freqSl50Hz->setTickInterval(50);
    freqSl50Hz->setSingleStep(1);
    freqSl50Hz->setPageStep(10);
    gridLayout->addWidget(freq50HzTxt, 0, 3, 1, 1);
    gridLayout->addWidget(freqSl50Hz, 1, 3, 1, 1);

    freqSl2MHz->setValue(3); freqSl10kHz->setValue(3); freqSl50Hz->setValue(3);
    freqComponent[0]=3*10000; freqComponent[1]=3*50; freqComponent[2]=3*0.25;                           // calc absolute frequency value
    genOp->freq=freqComponent[0]+freqComponent[1]+freqComponent[2];                           // calc absolute frequency value


    QObject::connect(freqSl2MHz, SIGNAL(valueChanged(int)), this, SLOT(rcvDialFreqLevel(int)));     //2MHz slider -> receive slot
    QObject::connect(freqSl10kHz, SIGNAL(valueChanged(int)), this, SLOT(rcvDialFreqLevel(int)));    //10 kHz slider -> receive slot
    QObject::connect(freqSl50Hz, SIGNAL(valueChanged(int)), this, SLOT(rcvDialFreqLevel(int)));     //50 Hz slider -> receive slot
    QObject::connect(this, SIGNAL(freqChanged(QString)), freqLabel, SLOT(setText(QString)));        //receive slots signal -> label

    // #####################################################  offset  ###############
    QLabel *offsTxt = new QLabel("Offs:", this);
    offsTxt->setMaximumWidth(35);

    QSlider *offsSldr = new QSlider(this);
    offsSldr->setMaximum(255);
    offsSldr->setOrientation(Qt::Vertical);
    offsSldr->setTickPosition(QSlider::TicksAbove);
    offsSldr->setTickInterval(16);
    offsSldr->setPageStep(1);


    offsLbl = new QLabel("   ", this);
    offsLbl->setMinimumWidth(45);
    offsLbl->setMaximumWidth(45);
    offsLbl->setAlignment(Qt::Alignment(Qt::AlignRight));

    QHBoxLayout *offsLblLayout = new QHBoxLayout();
    offsLblLayout->addWidget(offsTxt);
    offsLblLayout->addWidget(offsLbl);
    offsLblLayout->setSpacing(0);
    gridLayout->addLayout(offsLblLayout , 0, 5, 1, 1);
    gridLayout->addWidget(offsSldr,1, 5, Qt::AlignHCenter);

    offsSldr->setValue(0x80); offsLbl->setText("0.0 V"); genOp->offsVal=0x80;                  //setting slider pos, label txt and genVal

    QObject::connect(offsSldr, SIGNAL(valueChanged(int)), this, SLOT(rcvDialOffsLevel(int)));   //slider -> receive slot
    QObject::connect(this, SIGNAL(offsChanged(QString)), offsLbl, SLOT(setText(QString)));      //receive slots signal -> label



    // #####################################################  amplitude  ########gridLayout->addWidget(freqTLabel, 0, 0, 1, 1);#######
    QLabel *amplTLabel = new QLabel("Amplitude:",this);
    QLabel *amplLabel = new QLabel("0.11564 V",this);

    //amplCoTxt->setMaximumWidth(35);
    //amplCoLbl->setMaximumWidth(45);
    //amplCoLbl->setMinimumWidth(45);
    gridLayout->addWidget(amplTLabel, 2, 0, 1, 1);
    gridLayout->addWidget(amplLabel, 3, 0, 1, 1);
    //amplCoLbl->setAlignment(Qt::Alignment(Qt::AlignRight));

    QLabel *amplCoTxt = new QLabel("coarse               ", this);
    QSlider *amplCoSldr = new QSlider(this);
    amplCoSldr->setObjectName("amplSldrCoarse");
    amplCoSldr->setMinimum(0x00); //0x08
    amplCoSldr->setMaximum(0x07); //0x0f
    amplCoSldr->setValue(0x03); genOp->amplCoarse=0x03;
    amplCoSldr->setOrientation(Qt::Vertical);
    amplCoSldr->setTickPosition(QSlider::TicksAbove);
    amplCoSldr->setTickInterval(2);
    amplCoSldr->setPageStep(1);
    amplCoSldr->setSingleStep(1);
    gridLayout->addWidget(amplCoTxt, 2, 1, 1, 1);
    gridLayout->addWidget(amplCoSldr, 3, 1, 1, 1);


    QLabel *amplFnTxt = new QLabel("fine                  ", this);
    QSlider *amplFnSldr = new QSlider(this);
    amplFnSldr->setObjectName("amplSldrFine");
    amplFnSldr->setMinimum(0x00);
    amplFnSldr->setMaximum(0x07);
    amplFnSldr->setValue(0x03); genOp->amplFine=0x03;
    amplFnSldr->setOrientation(Qt::Vertical);
    amplFnSldr->setTickPosition(QSlider::TicksAbove);
    amplFnSldr->setTickInterval(2);
    amplFnSldr->setPageStep(1);
    amplFnSldr->setSingleStep(1);
    gridLayout->addWidget(amplFnTxt, 2, 2, 1, 1);
    gridLayout->addWidget(amplFnSldr, 3, 2, 1, 1);


    QLabel *amplSwTxt = new QLabel("soft                  ", this);
    QSlider *amplSwSldr = new QSlider(this);    amplCoLbl = new QLabel("   ", this);
    amplSwSldr->setObjectName("amplSldrSoft");
    amplSwSldr->setMinimum(0x00);
    amplSwSldr->setMaximum(0xff);
    amplSwSldr->setValue(0x7f); genOp->amplSoft=0x7f;
    amplSwSldr->setOrientation(Qt::Vertical);
    amplSwSldr->setTickPosition(QSlider::TicksAbove);
    amplSwSldr->setTickInterval(32);
    amplSwSldr->setPageStep(1);
    amplSwSldr->setSingleStep(1);
    gridLayout->addWidget(amplSwTxt, 2, 3, 1, 1, Qt::AlignLeft);
    gridLayout->addWidget(amplSwSldr, 3, 3, 1, 1);


    QObject::connect(amplCoSldr, SIGNAL(valueChanged(int)), this, SLOT(rcvDialAmplLevel(int)));   //sliderCoarse -> receive slot
    QObject::connect(amplFnSldr, SIGNAL(valueChanged(int)), this, SLOT(rcvDialAmplLevel(int)));   //sliderFine -> receive slot
    QObject::connect(amplSwSldr, SIGNAL(valueChanged(int)), this, SLOT(rcvDialAmplLevel(int)));   //sliderSoft -> receive slot
    QObject::connect(this, SIGNAL(amplChanged(QString)), amplLabel, SLOT(setText(QString)));      //receive slots signal -> label
    calcAmplitudeDisplay();

    // #####################################################  byte 5  ###############
    /*QLabel *byte5Lbl = new QLabel("Byte 5", this);
    gridLayout->addWidget(byte5Lbl,2,4,1,1);

    QSpinBox *byte5SPB = new QSpinBox(this); byte5SPB->setRange(0x00, 0xff);
    gridLayout->addWidget(byte5SPB, 3, 4, 1, 1);

    byte5SPB->setValue(0x10); genOp->byte5=0x10;                                                //setting spin box and genVal

    QObject::connect(byte5SPB, SIGNAL(valueChanged(int)), this, SLOT(rcvDialByte5(int)));*/       //spin box -> receive slot


    // #####################################################  byte 6 ###############
    QLabel *byte6Lbl = new QLabel("Byte 6", this);
    gridLayout->addWidget(byte6Lbl,2,5,1,1);

    QSpinBox *byte6SPB = new QSpinBox(this); byte6SPB->setRange(0x00, 0xff);
    gridLayout->addWidget(byte6SPB, 3, 5, 1, 1);

    byte6SPB->setValue(0x09); genOp->byte6=0x09;                                                //setting spin box and genVal

    QObject::connect(byte6SPB, SIGNAL(valueChanged(int)), this, SLOT(rcvDialByte6(int)));       //spin box -> receive slot



    // #####################################################  signal form ###############
    geV = new genView();                                // instantialise generator View
    geV->initSeries(devHandle->genSampleBuffer);        // set signal form to display (sin)

    gridLayout->addWidget(geV->getChartView(), 4,0,1, 4);
    sigFormGLayout = new QGridLayout;

    sigFormLbls = new QLabel [5];
    (sigFormLbls+0)->setText("dc");
    (sigFormLbls+1)->setText("sin");
    (sigFormLbls+2)->setText("rect");
    (sigFormLbls+3)->setText("saw");
    (sigFormLbls+4)->setText("sinc");

    sigFormRBs = new QRadioButton [5];
    (sigFormRBs + 1)->setChecked(true);

    sigFormRBG = new QButtonGroup;
    for (int l=0; l < 5; l++){
        sigFormRBG->addButton(sigFormRBs+l, l);
        sigFormGLayout->addWidget(sigFormRBs + l, l, 0, 1, 1); sigFormGLayout->addWidget(sigFormLbls + l, l, 1, 1, 1);
    }
    QObject::connect(sigFormRBG, SIGNAL(buttonClicked(int)), this, SLOT(rcvsigFormRBG(int)));   //radio button group -> this->slot
    QObject::connect(genOp, SIGNAL(genSigFormIsSet()), this, SLOT(updateSigView()));            //gen operator::signal form sample maker -> this->updateSigViw

    gridLayout->addLayout(sigFormGLayout, 4,4,1,2);
}

genWin::~genWin(){
    delete [] sigFormRBs;
    delete [] sigFormLbls;
}

void genWin::rcvDialFreqLevel(int val){     // slot for handling three frequency sliders
    double frequency;
    QString freqText;
    // std::cout << "rcvDialFreqLevel(val):" << val << " -> ";
    if (sender()->objectName() == "slider2MHz") { freqComponent[0]=10000.0*(float) val; }   // 2 MHz slider (10 kHz step width)
    if (sender()->objectName() == "slider10kHz"){ freqComponent[1]=50.0*(float) val; }      //10 kHz slider (50 Hz step width)
    if (sender()->objectName() == "slider50Hz") { freqComponent[2]=0.25*(float) val; }      // 50 Hz slider (0.25 Hz step width)
    frequency=freqComponent[0]+freqComponent[1]+freqComponent[2];                           // calc absolute frequency value
    genOp->freq=frequency; genOp->start();                                                  // send frequency value to gen operator thread

    if (frequency >= 1e6) {freqText.setNum(frequency*1e-6); freqText.append(" MHz");}
    if (frequency < 1e6 && frequency >=1e3) {freqText.setNum(frequency*1e-3); freqText.append(" kHz");}
    if (frequency < 1e3) {freqText.setNum(frequency); freqText.append(" Hz");}
    if (frequency < 0.1) {frequency=0.0001;}
    emit freqChanged(freqText);                                                             // set text of frequency label
   //  std::cout << freqText.toStdString() << std::endl;
}

void genWin::rcvDialAmplLevel(int val){     // slot for handling amplitude slider

    if (sender()->objectName() == "amplSldrCoarse"){
        genOp->amplCoarse=val; genOp->start();   //use 3 least significant bits and set all others to 0
    }
    if (sender()->objectName() == "amplSldrFine") {
        genOp->amplFine=val; genOp->start();     //use 3 least significant bits and set all others to 0
    }
    if (sender()->objectName() == "amplSldrSoft") {
        genOp->amplSoft=val; genOp->start();                  // val: 0x00 .. 0xff
    }
    calcAmplitudeDisplay();
}

void genWin::calcAmplitudeDisplay(){
    float amplComponent[3];
    amplComponent[0]=(float)qPow(0.5, 7.0 -(qreal)genOp->amplCoarse) ;                // val: 0x00 .. 0x0
    amplComponent[1]=0.01 * (float)genOp->amplFine + 0.93;           // val: 0x00 .. 0x07
    amplComponent[2]=((float)genOp->amplSoft)/255.0;

    QString amplText = QString("%1 V").arg(5.47*amplComponent[0]*amplComponent[1]*amplComponent[2], -3, 'f', 3);
    emit amplChanged(amplText);
}


void genWin::rcvDialOffsLevel(int val){     // slot for handling offset slider
    double offsVltg= 5.2*(((double)val)/128.0 -1.0); //val+1 bc of shift in offs vltg
    genOp->offsVal=val;    genOp->start();

    QString offsText = QString("%1 V").arg(offsVltg, -3, 'f', 2);
    //QString offsText = QString("%1").arg(val);

    emit offsChanged(offsText);
}

void genWin::rcvDialByte5(int val){         // slot for handling byte 5 slider
    genOp->byte5=val;
    genOp->start();
}

void genWin::rcvDialByte6(int val){         // slot for handling byte 6 slider
    genOp->byte6=val;
    genOp->start();
}

void genWin::rcvsigFormRBG(int val){        // slot for handling radio button group (dc, sin, rect,...)

   // printf ("### genWin::rcvsigFormRBG(int) %d\n", val); fflush(stdout);
    genOp->genSigForm=val;                  // set signal selection in genOperator (seperate thread)
                                            // gen operator thread fills devHandle->genSampleBuffer with suitable sample data
                                            // and emits signal connectd to genWin::updateSigView() which updates signal form display in generator window
    genOp->start();                         // start thread to update data in device handler
}


void genWin::updateSigView(){               // slot for updating signal form display (connected to signal in get operator thread)
    // printf("### genWin::updateSigView()\n"); fflush(stdout);
    geV->updateSeries(devHandle->genSampleBuffer);  // update series in genView
}

void genWin::closeEvent(QCloseEvent *event) {   // event handler closing genertor window when oszi window is closed
    printf("close event... \n"); fflush(stdout);
    if (otherWin != nullptr){otherWin->close();}
}
