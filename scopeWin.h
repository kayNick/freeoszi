#pragma once

#include <QWidget>
#include <QMutex>
#include <QChart>
#include <QChartView>
#include <QValueAxis>
#include <QLineSeries>
#include <QXYSeries>
#include <QChartView>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QPushButton>
#include <QSlider>
#include <QtWidgets/QSpinBox>
#include <QVector>
#include <QPoint>
#include <QtMath>
#include <QTimer>
#include <QDebug>
#include <QtWidgets/QDial>



#include "deviceHandler.h"
#include "oszilloskop.h"
#include "fftEngine.h"
#include "nextSampleWaitTimer.h"
#include "scopeView.h"
#include "specView.h"


QT_CHARTS_BEGIN_NAMESPACE
class QLineSeries;
class QChart;
QT_CHARTS_END_NAMESPACE
QT_CHARTS_USE_NAMESPACE


class scopeWin : public QWidget
{
    Q_OBJECT

public:
    explicit scopeWin(deviceHandler *dH, fftEngine *fE, QWidget *parent = nullptr);
    ~scopeWin();
private:
    void initView();
    void getCalDataFromStore();

public slots:
    void initDevice();
    void rcvStartRunning();
    void rcvRunOnce();
    void rcvStopRunning();
    void rcvStartSimulating();
    void rcvDialValCh1(int);
    void rcvDialValCh2(int);
    void couplingSelectCh1(int);
    void couplingSelectCh2(int);
    void rcvDialTimeBase(int);
    void rcvDialOffsCh1(int);
    void rcvDialOffsCh2(int);
    void rcvDialTriggerLevel(int);
    void triggerChanSelect(int);
    void triggerEdgeSelect(int);
    void triggerEnabled(int);
    void rcvCalibrate();
    void rcvCalibrationDone(int);
    void rcvCalibrationRestored();
    void selectView(int);




signals:
    void selVpDivCh2 (double);
    void selTimeBase ();
    void selTriggerLevel(double);
    void setTimeBaseText(QString);
    void updateMeanCh1(double); void updateMeanCh2(double);
    void updateRMSCh1(double); void updateRMSCh2(double);

private:
    deviceHandler *devHandle;
    nextSampleWaitTimer *waitLabelTimer;
    oszilloskop *oszi;
    fftEngine *fftE;


    QDial *dialCh1, *dialCh2, *dialTrigLev, *dialTimeBase;
    QVBoxLayout *mainLayout;
    unsigned char sampleDataBuffer[8192];
    unsigned char tmp[8192]; // for undoing weird sample shift...
    scopeView *scV = nullptr;
    specView *fcV = nullptr;
    QObject  *actualViewObject=nullptr;
    QSpinBox *offsSbCh1=nullptr, *offsSbCh2=nullptr;
    QPushButton *cal;


public:
    QWidget *otherWin = nullptr; //just to be able to call close()

    void findMeanRMSTwoChan( unsigned char *buf, int len);

    QVBoxLayout * guiCh1Knobs();
    QVBoxLayout * guiCh2Knobs();
    QVBoxLayout * guiTimebaseKnobs();
    QVBoxLayout * guiTriggerKnobs();
    QChartView * setupScopeView();
    QChartView * setupSpecView();
protected:
//    void resizeEvent(QResizeEvent *event) override;
    void closeEvent(QCloseEvent *) override;
};
