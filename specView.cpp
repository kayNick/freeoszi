#include "specView.h"

specView::specView(oszilloskop *oszi, qreal *seneitivityCh1, qreal *sensitivityCh2, deviceHandler *dH, fftEngine *fE, int fs):  devHandle(dH), fftE(fE){
    maxCh1=4.0 * *seneitivityCh1; maxCh2=4.0 * *sensitivityCh2;
    f_chart=new QChart;
    fView = new QChartView(f_chart);
    fView->setMinimumSize(600, 450);
    fView->setContentsMargins(0,0,0,0);
    f_chart->setWindowFrameMargins(0,0,0,0);
    f_chart->setContentsMargins(0,0,0,0);

    ch1_SpecSeries = new QLineSeries;
    ch2_SpecSeries = new QLineSeries;

    f_chart->addSeries(ch1_SpecSeries);
    f_chart->addSeries(ch2_SpecSeries);
    axisFreq = new QValueAxis;
    axisFreq->setRange(0, 2048);
    axisFreq->setTickCount(9);
    axisFreq->setLabelFormat("%g");
    axisFreq->setTitleText("Frequency");


    QValueAxis *axisMag = new QValueAxis;
    axisMag->setRange(-88, 22);
    axisMag->setTickCount(12);
    axisMag->setLabelFormat("%g");
    axisMag->setTitleText("dBV");


    f_chart->addAxis(axisFreq, Qt::AlignBottom);
    ch1_SpecSeries->attachAxis(axisFreq);
    ch2_SpecSeries->attachAxis(axisFreq);

    f_chart->addAxis(axisMag, Qt::AlignLeft);
    ch1_SpecSeries->attachAxis(axisMag);
    ch2_SpecSeries->attachAxis(axisMag);

    f_chart->legend()->hide();
    f_chart->setTitle("Spectrum");
    ch1SQVec = new QVector<QPointF>;
    ch2SQVec = new QVector<QPointF>;
    this->updatefRange(fs);

    memcpy(tmp, devHandle->rdBuf, 8192);
    memcpy(sampleDataBuffer, tmp+8129, 63); memcpy(sampleDataBuffer+63, tmp, 8129); //some kind of weird glitch in data handling

    for (int l=0; l<4096; l++){
        fftE->inCh1[l][0] = (double) maxCh1 *( ((double) *(sampleDataBuffer+ 2*l  ))/127.5-1.0);  // feed sample data into fft input buffer
        fftE->inCh2[l][0] = (double) maxCh2 *( ((double) *(sampleDataBuffer+ 2*l+1))/127.5-1.0);  // feed sample data into fft input buffer
    }
        fftE->exec();

    for (int k=0; k<2048; k++){
        //ch1SQVec->append(QPointF((qreal)k, (qreal) fftE->outCh1[k][0] ));
        //ch2SQVec->append(QPointF((qreal)k, (qreal) fftE->outCh2[k][0] ));
        ch1SQVec->append(QPointF((qreal)k*df,   20*0.4342944819032518*qLn(qSqrt( qPow(1/2048.0* ((qreal) fftE->outCh1[k][0]),2) + qPow(1/2048.0*((qreal) fftE->outCh1[k][1]),2) )) ));
        ch2SQVec->append(QPointF((qreal)k*df,   20*0.4342944819032518*qLn(qSqrt( qPow(1/2048.0* ((qreal) fftE->outCh2[k][0]),2) + qPow(1/2048.0*((qreal) fftE->outCh2[k][1]),2) )) ));
    }
    ch1_SpecSeries->replace(*ch1SQVec);
    ch2_SpecSeries->replace(*ch2SQVec);
    if (oszi->couplingCh1_dialVal == 4) ch1_SpecSeries->setVisible(false);
    if (oszi->couplingCh2_dialVal == 4) ch2_SpecSeries->setVisible(false);

    author = new QGraphicsSimpleTextItem(f_chart);
    author->setText("by K. Nick\ndr.k.nick@gmx.de");

    ovL = new QGraphicsSimpleTextItem(f_chart);
    ovL->setText("OL");
    ovL->setPos(150, 20);
    ovL->setBrush(QBrush(Qt::red));
    ovL->hide();
    connect(f_chart, SIGNAL(plotAreaChanged(const QRectF &)), this, SLOT(updateOnResize(const QRectF &)));

}

specView::~specView(){

    delete f_chart;
}
QChartView * specView::getChartView(){
    return fView;
}
void specView::updateYRangeCh1(double sensitivityCh1){
    maxCh1=4*sensitivityCh1;
}
void specView::updateYRangeCh2(double sensitivityCh2){
    maxCh2=4*sensitivityCh2;
}
void specView::setVisibleCh1(bool visibleCh1){
    ch1_SpecSeries->setVisible(visibleCh1);
}
void specView::setVisibleCh2(bool visibleCh2){
    ch2_SpecSeries->setVisible(visibleCh2);
}

void specView::updateSeries(){
    //unsigned char tmp[8192]; // for undoing weird sample shift...

    //printf("specView updateSeries()...\n"); fflush(stdout);
    if (devHandle->rdBufMutex->try_lock()){
        memcpy(tmp, devHandle->rdBuf, 8192);
        devHandle->rdBufMutex->unlock();
    }
    memcpy(sampleDataBuffer, tmp+8129, 63); memcpy(sampleDataBuffer+63, tmp, 8129); //some kind of weird glitch in data handling requires putting last 63 bytes at the beginning of the data buffer
    /*for (int l=0; l<4096; l++){
        ch1SQVec->replace(l, QPointF( (qreal)l, maxCh1 * (((qreal) *(sampleDataBuffer+ 2*l  ) )/127.5-1.0)  )  );
        ch2SQVec->replace(l, QPointF( (qreal)l, maxCh2 * (((qreal) *(sampleDataBuffer+ 2*l+1) )/127.5-1.0)  )  );
         if ( *(sampleDataBuffer+ 2*l) % 255 == 0 || *(sampleDataBuffer+ 2*l+1) % 255 == 0 ){this->showOvL();}
      }*/

    calcSpec();
    ch1_SpecSeries->replace(*ch1SQVec);
    ch2_SpecSeries->replace(*ch2SQVec);
    emit  updateMeanRMS(sampleDataBuffer, 4096);

}
void specView::calcSpec(){  //thi is a stub for now...
    //float max1=-2000, max2=-2000;
    for (int l=0; l<4096; l++){
        fftE->inCh1[l][0] = (double) maxCh1 *( ((double) *(sampleDataBuffer+ 2*l  ))/127.5-1.0);  // feed sample data into fft input buffer
        fftE->inCh2[l][0] = (double) maxCh2 *( ((double) *(sampleDataBuffer+ 2*l+1))/127.5-1.0);  // feed sample data into fft input buffer
    if ( *(sampleDataBuffer+ 2*l) % 255 == 0 || *(sampleDataBuffer+ 2*l+1) % 255 == 0 ){this->showOvL();}
    }
    fftE->exec();

    for (int k=0; k<2048; k++){
        //ch1SQVec->replace(k, QPointF((qreal)k,   qSqrt( qPow(1/2048.0* ((qreal) fftE->outCh1[k][0]),2) + qPow(1/2048.0*((qreal) fftE->outCh1[k][1]),2) ) ));
        //ch2SQVec->replace(k, QPointF((qreal)k,   qSqrt( qPow(1/2048.0* ((qreal) fftE->outCh2[k][0]),2) + qPow(1/2048.0*((qreal) fftE->outCh2[k][1]),2) ) ));
        ch1SQVec->replace(k, QPointF((qreal)k*df,   20*0.4342944819032518*qLn(qSqrt( qPow(1/2048.0* ((qreal) fftE->outCh1[k][0]),2) + qPow(1/2048.0*((qreal) fftE->outCh1[k][1]),2) )) ));
        ch2SQVec->replace(k, QPointF((qreal)k*df,   20*0.4342944819032518*qLn(qSqrt( qPow(1/2048.0* ((qreal) fftE->outCh2[k][0]),2) + qPow(1/2048.0*((qreal) fftE->outCh2[k][1]),2) )) ));

        //max1=fmax(max1, ch1SQVec->at(k).y()); max2=fmax(max2, ch2SQVec->at(k).y());
        // printf("max1: %f, max2: %f, %f\n", max1, max2, max2/max1); fflush(stdout);
    }

}
void specView::updatefRange(int fs){

    if (fs < 2000){
        axisFreq->setRange(0, fs/2.0);
        df=fs/2.0/2048;
        axisFreq->setTitleText("Frequency Hz");
    }
    else if (fs >= 2000 &&  fs <= 2000000){
        axisFreq->setRange(0, fs/2.0/1000);
        df=fs/2.0/1000/2048;
        axisFreq->setTitleText("Frequency kHz");
    }
    else if (fs >= 2000000){
        axisFreq->setRange(0, fs/2.0/1000000);
        df=fs/2.0/1000000/2048;
        axisFreq->setTitleText("Frequency MHz");
    }
}
void specView::showOvL(){
    if (! ovL->isVisible()){
        ovL->show();
        QTimer::singleShot(5000, this, SLOT(hideOvL()));
    }
}
void specView::hideOvL(){
    ovL->hide();
}
void specView::updateOnResize(const QRectF &rect){
    //printf("scopeView::updateOnResize\n"); fflush(stdout);
    author->setPos(f_chart->geometry().width()-200,10);
}
