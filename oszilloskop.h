#ifndef OSZILLOSKOP_H
#define OSZILLOSKOP_H
#pragma once

#include <QThread>
#include <QString>
#include <QLabel>
#include <QtMath>
#include <QSettings>
#include <QVariant>
#include <QByteArray>
#include "deviceHandler.h"
#include "nextSampleWaitTimer.h"

class oszilloskop : public QThread{
    Q_OBJECT

public:
    explicit oszilloskop(deviceHandler *, nextSampleWaitTimer*);
    ~oszilloskop();
    void run();

    int sensivCh1_dialVal=2, sensivCh2_dialVal=2;                   // sensitivity dial value (int) for channel 1 & 2
    qreal sensitivityCh1=0.3, sensitivityCh2=0.3;                   // sensitivity for channel 1 & 2
    qreal *sensitivityTrigChan = &sensitivityCh1;                   // sensitivity of channel used for triggering
    int offsCh1_dialVal=0, offsCh2_dialVal=0;                       // offset for channel 1 & 2
    int couplingCh1_dialVal=1, couplingCh2_dialVal=1;               // coupling of channel 1 & 2, this holds on/off state (visibility) of each channel (needed to disable a channel trace)
    int trigLevel_dialVal=0x7f;                                     // trigger level dial value (int)
    qreal trigLevel=0.0;                                            // trigger level dial value (int)
    int trigChanSel_dialVal=1, trigEdge_dialVal=1, trigEnabled_dialVal=Qt::Unchecked; // trigger channel, edge and on/off setting
    int timeBase_dialVal=10; int fs=625000;                         // sampling time base and corresponding sampling frequency
    char tbText[10]="625 kS/s"; qreal dt=0.0016;                    // time base text representation corresponding to sampling frequenca
    int actionToPerform;
    bool calibrationInPorgress = false;
    enum availableActions{initialize, setSensitivityCh1, setSensitivityCh2, setOffsetCh1, setOffsetCh2, setCouplingCh1, setCouplingCh2,
                         setTriggerLevel, setTriggerChannel, setTriggerEdge, setTriggerEnable, setTimeBase, startRunning, runOnce, startSimulating,  calibrate, storeCalData, getCalDataFromStore};
    enum calResults{cal_success, cal_fail, cal_deviceBusy};
    //void testfunc();
private:
    deviceHandler *devHandle;
    nextSampleWaitTimer *waitLabelTimer;
    bool waitingForSampleSignalConneced=false;
    QSettings *calDataSettings;
    void m_initialize();
    void m_setSensitivityCh1();
    void m_setSensitivityCh2();
    void m_setOffsetCh1();
    void m_setOffsetCh2();
    void m_setCouplingCh1();
    void m_setCouplingCh2();
    void m_setTriggerLevel();
    void m_setTriggerChannel();
    void m_setTriggerEdge();
    void m_setTriggerEnable();
    void m_setTimeBase();
    void m_startRunning();
    void m_runOnce();
    void m_startSimulating();
    void m_calibrate();
    void m_storeCalData();
    void m_getCalDataFromStore();

    void m_checkNextSampleWaitTimerEnable();
    void m_findMeanTwoChan(double *meanCh1, double *meanCh2, unsigned char *buf, int len);
    void m_findOffset(unsigned char *offsCh1, unsigned char *offsCh2);


public slots:


signals:
    void sensitivityCh1Changed(qreal);
    void sensitivityCh2Changed(qreal);
    void visibilityCh1Changed(bool);
    void visibilityCh2Changed(bool);
    void triggerStateChanged(int);
    void triggerLevelChanged(qreal);
    void triggerChannelChanged(int);
    void triggerEdgeChanged();

    void timeBaseKnobTextChanged(QString);
    void timeBaseChanged(qreal, qreal, QString);
    void waitLabelTimerOnOff(bool, int);
    void calibrationDone(int);
    void calibrationDataRestored();
};

#endif // OSZILLOSKOP_H
