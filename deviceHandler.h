#pragma once

#include <QThread>
#include <QMutex>
#include <QDebug>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>



extern "C" {
#include <libusb-1.0/libusb.h>
// #include <pthread.h>
}
class oszilloskop;

class deviceHandler : public QThread
{
    Q_OBJECT
public:
    explicit deviceHandler();
    ~deviceHandler();
        QString Name;
        QMutex *rdBufMutex;


    bool runGranted = false;
    int i=0;

signals:
    void waitingForSample();
    void sampleDataReady();


public:
    // void (oszilloskop::*funcPointer)();
    // funcPointer = &oszilloskop::testfunc;
    // for general usb handling
    libusb_device **devs = NULL, *corr_device = NULL;
    libusb_context *context = NULL;
    libusb_device_handle *devhandle = NULL;
    size_t num_of_devices_found;
    struct libusb_device_descriptor desc;
    struct libusb_config_descriptor *config;

    // for scope handling
    unsigned char confStrBuf[100];
    unsigned char rdBuf[8192];
    int funcToRun=0;    //switch which functionality to exec (init() =0, calibrate() = 10, runCont() = 20, runOnce() = 21, ...
    enum devActions{initialze=0, run_calibration=10, sample_continuously=20, sample_once=21, simulate_continuously=31, reset_device=101, run_testFunc};
    unsigned char CmdBuf[10];
    bool initComplete=false;
    bool CmdHasChanged=false;
    //unsigned char calOffsCh1[6]={0x73,0x74,0x73,0x74,0x73,0x74}, calOffsCh2[6]={0x72,0x71,0x72,0x71,0x72,0x71}; // default cal data if no cal data has been found (no calibration done yen)
    unsigned char calOffsCh1[6]={0x23,0x54,0x43,0x14,0x13,0x14}, calOffsCh2[6]={0x12,0x11,0x12,0x11,0x12,0x11}; // default cal data if no cal data has been found (no calibration done yen)

    // for generator handling
    unsigned char genCmdBuf[7] = {0x0e, 0x05, 0x04, 0x7f, 0x4f, 0x74, 0x71};
    unsigned char genFreqCmdBuf[22] = {0x0e,0x02,0x13,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x23,0xd6,0xe2,0x53,0x00,0x00,0xa0,0x86,0x01,0x00,0x00};
    bool genCmdHasChanged=false;
    bool scopeIsRunning = false;

    //functions
    void run(); //device handler thread  entry point
    int readfrdev(libusb_device_handle *devhandle, unsigned char *rBuf, int len);
    int write2dev(libusb_device_handle *devhandle, unsigned char *wBuf, int len);


    void stopdev(libusb_device_handle *devhandle);
    void printCmdBuf();
    void printgenCmdBuf();
    void genCmdSend();


private:

    void runCont();
    void runOnce();
    void simulate();
    void initLibUSB();
    libusb_device* _find_correct_device(libusb_device **devs, size_t num_of_devices_found);
    int initDevice();
    void resetDev();

private:
#include "fw1.h" // unsigned char fw1Str[54912]
public:
#include "genSampleBuffer.h" //unsigned char genSampleBuffer[512]


};
