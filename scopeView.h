#ifndef SCOPEVIEW_H
#define SCOPEVIEW_H

#include <QChart>
#include <QChartView>
#include <QLineSeries>
#include <QValueAxis>
#include <QPointF>
#include <QtMath>
#include <QTimer>
#include "oszilloskop.h"
#include "deviceHandler.h"


QT_CHARTS_BEGIN_NAMESPACE
class QLineSeries;
class QChart;
QT_CHARTS_END_NAMESPACE
QT_CHARTS_USE_NAMESPACE

class trigEngine;

class scopeView : public QObject{
   Q_OBJECT

public:
    scopeView(oszilloskop *oszi,  deviceHandler *dH);
    ~scopeView();
    QChartView * getChartView();
    qreal dt; //this is set in initialiser list

public slots:
        void updateSeries();
        void setYRangeCh1(qreal);
        void setYRangeCh2(qreal);
        void setVisibleCh1(bool);
        void setVisibleCh2(bool);
        void hideOvL();
        void showOvL();
        void updateOnResize(const QRectF &rect);
        void setTriggerState(int);
        void setTriggerLevel(qreal);
        void setTriggerChannel(int);
        void setTimeBase(qreal, qreal, QString);
signals:
        void updateMeanRMS(unsigned char *, int);
private:
        deviceHandler *devHandle;
        QChart *m_chart;
        QChartView *tView;
        QLineSeries *ch1_series, *ch2_series;
        QValueAxis *axisYCh1, *axisYCh2,  *axisTime;
        QVector<QPointF> *ch1QVec, *ch2QVec;
        trigEngine *tE;
        unsigned char sampleDataBuffer[8192];
        unsigned char tmp[8192]; // for undoing weird sample shift...
        qreal maxCh1, maxCh2;
        QGraphicsSimpleTextItem *ovL;
        QGraphicsSimpleTextItem *author, *ch1AxisLbl, *ch2AxisLbl;

};

//  ------- class trigstate from here on -----------------------------------------------------------------

class trigEngine : public QObject{
    Q_OBJECT
private:
    bool enabled = false;
    QChart *m_chart;
    QPointF ptsAxis[2];         // points representing beginning and end of trigger marker, in axis units
    QPointF ptsAbs[2];          // points representing beginning and end of trigger marker, in absolute units
    QLineSeries *m_1series, *m_2series, *akt_series;
    QLineF *qLF;
    QGraphicsLineItem *m_gLineItem = nullptr;
    qreal dt, level;

public:
    trigEngine(QChart *_chart, QLineSeries *_1series, QLineSeries *_2series, qreal *_dt) : m_chart(_chart),
                m_1series(_1series), m_2series(_2series), qLF(new QLineF), m_gLineItem(new QGraphicsLineItem(m_chart)), dt(*_dt){
        akt_series = m_1series;

        level=0.0;        // init trigger marker level
        m_gLineItem->setPen(QPen(Qt::blue, 3));     // init trigger marker color (to chan 1)
        m_gLineItem->hide();    // do not show marker as initially trigger is disabled
        QTimer::singleShot(1, this, SLOT(mapToPositionDelay()));
    };

    void setLevel(qreal lev=0){// level in y-axis coordinates
        level = lev;
        update();
    }
    void setChan(int chan){// set chan (color) and reference y-axis for correct placing trigger marker
        switch(chan){
        case 1: {
            akt_series = m_1series;
            m_gLineItem->setPen(QPen(Qt::blue, 3));
            break;}
        case 2: {
            akt_series = m_2series;
            m_gLineItem->setPen(QPen(Qt::darkGreen, 3));
            break;}
        }
        update();
    }
    void setTimeBase(qreal dt){
        this->dt=dt;
        update();
    }
    void enable(){// show trigger marker when triggering enabled
        m_gLineItem->show();
        enabled = true;
        //update();
    }
    void disable(){// hide trigger marker when triggering disabled
        m_gLineItem->hide();
        enabled = false;
    }
    void runUpdate(){
        update();
    }
private:
    void update(){ // this creates line for trigger marker (from points)
        //printf("trigEngine::update: dt: %fl, level: %fl\n", *dt, level); fflush(stdout);
        ptsAxis[0].setX(944*dt); ptsAxis[0].setY(level);   // trigger marker start point, in t-axis coordinates
        ptsAxis[1].setX(1104*dt); ptsAxis[1].setY(level);  // tigger marker end point, in t-axis coordinates
        //printf("ptsAxis[0]: x:%fl, y:%fl, ptsAxis[1] x:%fl, y:%fl\n", ptsAxis[0].x(), ptsAxis[0].y(), ptsAxis[1].x(), ptsAxis[1].y()); fflush(stdout);

        ptsAbs[0] = m_chart->mapToPosition(ptsAxis[0], akt_series); // trigger marker start point, in abs coor (from t-axis coordinates)
        ptsAbs[1] = m_chart->mapToPosition(ptsAxis[1], akt_series); // trigger marker end point, in abs coor (from t-axis coordinates)
        //printf("ptsAbs[0]: x:%fl, y:%fl, ptsAbs[1] x:%fl, y:%fl\n", ptsAbs[0].x(), ptsAbs[0].y(), ptsAbs[1].x(), ptsAbs[1].y()); fflush(stdout);
        //printf("akt_series: %p\n", akt_series);
        //printf("m_chart->parent(): %x %x %x %x %x\n", m_chart->parent(), m_chart->parentItem(), m_chart->parentLayoutItem(), m_chart->parentWidget(), m_chart->parentObject());fflush(stdout);

        //ptsAbs[0].setX(268.0714); ptsAbs[0].setY(244.5);   // trigger marker start point, in t-axis coordinates
        //ptsAbs[1].setX(302.8); ptsAbs[1].setY(244.5);  // tigger marker end point, in t-axis coordinates
        qLF->setPoints(ptsAbs[0], ptsAbs[1]);   // create Line from start and end point
        m_gLineItem->setLine(*qLF);
        //printf("qLF: x1:%fl, y1:%fl, x2:%fl, y2:%fl\n", qLF->x1(), qLF->y1(), qLF->x2(), qLF->y2()); fflush(stdout);
    }
public slots:
    void mapToPositionDelay(){
        update();
        }
};

#endif // SCOPEVIEW_H
