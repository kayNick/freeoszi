#include <iostream>
#include "oszilloskop.h"

oszilloskop::oszilloskop(deviceHandler *dH, nextSampleWaitTimer *nSWT): devHandle(dH),
    waitLabelTimer(nSWT), calDataSettings(new QSettings ("FreeOszi", "freeoszi")){
        //connect(devHandle, &deviceHandler::calibrationDone, this, &oszilloskop::sl_calibrationDone); // set Knobs after calibration
    // devHandle->execOnCalReady = (this->sl_calibrationDone);
}
oszilloskop::~oszilloskop(){}
void oszilloskop::run(){
    if (this->calibrationInPorgress){
        std::cout << "Can't do that! Calibration in progress..." << std::endl;
        return;
    }
    switch (actionToPerform){
    case oszilloskop::initialize: m_initialize(); break;
    case oszilloskop::setSensitivityCh1: m_setSensitivityCh1(); break;
    case oszilloskop::setSensitivityCh2: m_setSensitivityCh2(); break;
    case oszilloskop::setCouplingCh1: m_setCouplingCh1(); break;
    case oszilloskop::setCouplingCh2: m_setCouplingCh2(); break;
    case oszilloskop::setOffsetCh1: m_setOffsetCh1(); break;
    case oszilloskop::setOffsetCh2: m_setOffsetCh2(); break;
    case oszilloskop::setTriggerLevel: m_setTriggerLevel(); break;
    case oszilloskop::setTriggerChannel: m_setTriggerChannel(); break;
    case oszilloskop::setTriggerEdge: m_setTriggerEdge(); break;
    case oszilloskop::setTriggerEnable: m_setTriggerEnable(); break;
    case oszilloskop::setTimeBase: m_setTimeBase(); break;
    case oszilloskop::startRunning: m_startRunning(); break;
    case oszilloskop::runOnce: m_runOnce(); break;
    case oszilloskop::startSimulating: m_startSimulating(); break;
    case oszilloskop::calibrate: m_calibrate(); break;
    case oszilloskop::storeCalData: m_storeCalData(); break;
    case oszilloskop::getCalDataFromStore: m_getCalDataFromStore(); break;
    }
}
void oszilloskop::m_initialize(){
    if ( devHandle->initComplete){
        std::cout << "Device is initialized already!" << std::endl;
        return;
    }
    devHandle->funcToRun = deviceHandler::initialze;             // set what to do
    devHandle->start();                                          // start the device handler thread
}
void oszilloskop::m_setSensitivityCh1(){    // layout CmdBuf (10 bytes): _0e_ _80_ _07_ AmplCh1 AmplCh1 OffsCh1 OffsCh2 TrigLev TimeBase TrigType
    // Byte 3 -> Ch1: level and coupling       nu   nu  lev  GND  lev  lev  lev  cpl (DC=1 AC=0)
    // Byte 4 -> Ch2: level and coupling  bit: 7    6    5    4    3    2    1    0
    //                         level bit mask: 0    0    1    0    1    1    1    0 -> 0x2e inverted 0xd1
    //                           GND bit mask: 0    0    0    1    0    0    0    0 -> 0x10 inverted 0xef
    //                      coupling bit mask: 0    0    0    0    0    0    0    1 -> 0x01 inverted 0xfe

    // 0xd1: 11010001
    // 0x2e: 00101110
    // 0x22:   100010
    // 0x02:       10
    // 0x24:   100100
    // 0x04:      100
    // 0x28:   101000
    // 0x08:     1000

    //QThread::msleep(2000);
    int val=-1*sensivCh1_dialVal+5;
    switch (val){
    case 0:                     //set level bits (don't touch coupling bits) //set correct offset (from calibration)
        sensitivityCh1=0.01;
        devHandle->CmdBuf[3]=(devHandle->CmdBuf[3] & 0xd1) + (unsigned char) (0x22 & 0x2e);
        devHandle->CmdBuf[5]=(unsigned char)((int)devHandle->calOffsCh1[0]+offsCh1_dialVal);
        break;
    case 1:
        sensitivityCh1=0.03;
        devHandle->CmdBuf[3]=(devHandle->CmdBuf[3] & 0xd1) + (unsigned char) (0x02 & 0x2e);
        devHandle->CmdBuf[5]=(unsigned char)((int)devHandle->calOffsCh1[1]+offsCh1_dialVal);
        break;
    case 2:
        sensitivityCh1=0.10;
        devHandle->CmdBuf[3]=(devHandle->CmdBuf[3] & 0xd1) + (unsigned char) (0x24 & 0x2e);
        devHandle->CmdBuf[5]=(unsigned char)((int)devHandle->calOffsCh1[2]+offsCh1_dialVal);
        break;
    case 3:
        sensitivityCh1=0.30;
        devHandle->CmdBuf[3]=(devHandle->CmdBuf[3] & 0xd1) + (unsigned char) (0x04 & 0x2e);
        devHandle->CmdBuf[5]=(unsigned char)((int)devHandle->calOffsCh1[3]+offsCh1_dialVal);
        break;
    case 4:
        sensitivityCh1=1.00;
        devHandle->CmdBuf[3]=(devHandle->CmdBuf[3] & 0xd1) + (unsigned char) (0x28 & 0x2e);
        devHandle->CmdBuf[5]=(unsigned char)((int)devHandle->calOffsCh1[4]+offsCh1_dialVal);
        break;
    case 5:
        sensitivityCh1=3.00;
        devHandle->CmdBuf[3]=(devHandle->CmdBuf[3] & 0xd1) + (unsigned char) (0x08 & 0x2e);
        devHandle->CmdBuf[5]=(unsigned char)((int)devHandle->calOffsCh1[5]+offsCh1_dialVal);
        break;
    }
    emit sensitivityCh1Changed(sensitivityCh1);
    devHandle->CmdHasChanged=true;
}
void oszilloskop::m_setSensitivityCh2(){    // layout CmdBuf (10 bytes): _0e_ _80_ _07_ AmplCh1 AmplCh1 OffsCh1 OffsCh2 TrigLev TimeBase TrigType
    // Byte 3 -> Ch1: level and coupling       nu   nu  lev  GND  lev  lev  lev  cpl (DC=1 AC=0)
    // Byte 4 -> Ch2: level and coupling  bit: 7    6    5    4    3    2    1    0
    //                         level bit mask: 0    0    1    0    1    1    1    0 -> 0x2e inverted 0xd1
    //                           GND bit mask: 0    0    0    1    0    0    0    0 -> 0x10 inverted 0xef
    //                      coupling bit mask: 0    0    0    0    0    0    0    1 -> 0x01 inverted 0xfe
    int val=-1*sensivCh2_dialVal+5;
    switch (val){                           // set chosen amplitude CmdBuf[4] and corresponding offs level CmdBuf[6] for Ch2
        case 0:
            sensitivityCh2=0.01;
            devHandle->CmdBuf[4]=(devHandle->CmdBuf[4] & 0xd1) + (unsigned char) (0x22 & 0x2e);   //set level bits (don't touch coupling bits)
            devHandle->CmdBuf[6]=(unsigned char)((int)devHandle->calOffsCh2[0]+offsCh2_dialVal); //set correct offset (from calibration)
            break;
        case 1:
            sensitivityCh2=0.03;
            devHandle->CmdBuf[4]=(devHandle->CmdBuf[4] & 0xd1) + (unsigned char) (0x02 & 0x2e);
            devHandle->CmdBuf[6]=(unsigned char)((int)devHandle->calOffsCh2[1]+offsCh2_dialVal);
            break;
        case 2:
            sensitivityCh2=0.10;
            devHandle->CmdBuf[4]=(devHandle->CmdBuf[4] & 0xd1) + (unsigned char) (0x24 & 0x2e);
            devHandle->CmdBuf[6]=(unsigned char)((int)devHandle->calOffsCh2[2]+offsCh2_dialVal);
            break;
        case 3:
            sensitivityCh2=0.30;
            devHandle->CmdBuf[4]=(devHandle->CmdBuf[4] & 0xd1) + (unsigned char) (0x04 & 0x2e);
            devHandle->CmdBuf[6]=(unsigned char)((int)devHandle->calOffsCh2[3]+offsCh2_dialVal);
            break;
        case 4:
            sensitivityCh2=1.00;
            devHandle->CmdBuf[4]=(devHandle->CmdBuf[4] & 0xd1) + (unsigned char) (0x28 & 0x2e);
            devHandle->CmdBuf[6]=(unsigned char)((int)devHandle->calOffsCh2[4]+offsCh2_dialVal);
            break;
        case 5:
            sensitivityCh2=3.00;
            devHandle->CmdBuf[4]=(devHandle->CmdBuf[4] & 0xd1) + (unsigned char) (0x08 & 0x2e);
            devHandle->CmdBuf[6]=(unsigned char)((int)devHandle->calOffsCh2[5]+offsCh2_dialVal);
            break;
    }
    emit sensitivityCh2Changed(sensitivityCh2);
    devHandle->CmdHasChanged=true;
}
void oszilloskop::m_setOffsetCh1(){
    int val=-1*sensivCh1_dialVal+5;     // needed to figure out correct index in calibration array
    //printf("oszilloskop::m_setOffsetCh1(): %d\n", offsCh1_dialVal); fflush(stdout);
    devHandle->CmdBuf[5]=(unsigned char)((int)devHandle->calOffsCh1[val]+offsCh1_dialVal);
    devHandle->CmdHasChanged=true;
    //devHandle->printCmdBuf();
}
void oszilloskop::m_setOffsetCh2(){
    int val=-1*sensivCh2_dialVal+5;     // needed to figure out correct index in calibration array
    //printf("oszilloskop::m_setOffsetCh2(): %d\n", offsCh2_dialVal); fflush(stdout);
    devHandle->CmdBuf[6]=(unsigned char)((int)devHandle->calOffsCh2[val]+offsCh2_dialVal);
    devHandle->CmdHasChanged=true;
    //devHandle->printCmdBuf();
}
void oszilloskop::m_setCouplingCh1(){
    switch (couplingCh1_dialVal){                   // 0xee: 11101110 0x10: 10000
    case 1: devHandle->CmdBuf[3]=(devHandle->CmdBuf[3] & 0xee) + (unsigned char) 0x01; emit visibilityCh1Changed(true); break; //clear GND bit and DC coupling (set DC bit)
    case 2: devHandle->CmdBuf[3]=(devHandle->CmdBuf[3] & 0xee) + (unsigned char) 0x00; emit visibilityCh1Changed(true); break; //clear GND bit and AC coupling (clear DC bit)
    case 3: devHandle->CmdBuf[3]=(devHandle->CmdBuf[3] & 0xef) + (unsigned char) 0x10; emit visibilityCh1Changed(true); break; //GND (set GND bit)
    case 4: devHandle->CmdBuf[3]=(devHandle->CmdBuf[3] & 0xef) + (unsigned char) 0x10; emit visibilityCh1Changed(false); break; //GND (set GND bit) and set invisible
    }
    devHandle->CmdHasChanged=true;
}
void oszilloskop::m_setCouplingCh2(){
    switch (couplingCh2_dialVal){
    case 1: devHandle->CmdBuf[4]=(devHandle->CmdBuf[4] & 0xee) + (unsigned char) (0x01); emit visibilityCh2Changed(true); break; //clear GND bit and DC coupling (set DC bit)
    case 2: devHandle->CmdBuf[4]=(devHandle->CmdBuf[4] & 0xee) + (unsigned char) (0x00); emit visibilityCh2Changed(true); break; //clear GND bit and AC coupling (clear DC bit)
    case 3: devHandle->CmdBuf[4]=(devHandle->CmdBuf[4] & 0xef) + (unsigned char) (0x10); emit visibilityCh2Changed(true); break; //GND (set GND bit)
    case 4: devHandle->CmdBuf[4]=(devHandle->CmdBuf[4] & 0xef) + (unsigned char) (0x10); emit visibilityCh2Changed(false); break; //GND (set GND bit) and set invisible
    }
    devHandle->CmdHasChanged=true;
}
void oszilloskop::m_setTriggerLevel(){
    devHandle->CmdBuf[7]=(unsigned char) trigLevel_dialVal; // set Trig Lev for device
    devHandle->CmdHasChanged=true; // notify other thread that Cmd has been altered

    trigLevel= 4.0 * *sensitivityTrigChan *( ((qreal) trigLevel_dialVal)/127.5-1);
    emit triggerLevelChanged(trigLevel); //set correct value in trigger label
}
void oszilloskop::m_setTriggerChannel(){
    switch(trigChanSel_dialVal){
    case 1:{
        devHandle->CmdBuf[9]=(devHandle->CmdBuf[9] & 0xfe) + (unsigned char) (0x00 & 0x1); sensitivityTrigChan=&sensitivityCh1;  //ch1 (bit 0 disabled)
        //if (scV != nullptr) {scV->tS->setChan(1);}
        break;
        }
    case 2:{
        devHandle->CmdBuf[9]=(devHandle->CmdBuf[9] & 0xfe) + (unsigned char) (0x01 & 0x1); sensitivityTrigChan=&sensitivityCh2;  //ch2 (bit 0 emabled)
        //if (scV != nullptr) {scV->tS->setChan(2);}
        break;
        }
    }
    devHandle->CmdHasChanged=true;
    trigLevel= 4.0 * *sensitivityTrigChan *( ((qreal) trigLevel_dialVal)/127.5-1);
    emit triggerLevelChanged(trigLevel); //set correct value in trigger label
    emit triggerChannelChanged(trigChanSel_dialVal);
}
void oszilloskop::m_setTriggerEdge(){
    switch(trigEdge_dialVal){
    case 1: devHandle->CmdBuf[9]=(devHandle->CmdBuf[9] & 0xfb) + (unsigned char) (0x00 & 0x4); break; //rE  (bit 2 disabled)
    case 2: devHandle->CmdBuf[9]=(devHandle->CmdBuf[9] & 0xfb) + (unsigned char) (0x04 & 0x4); break; //fE (bit 2 emabled)
    }
    devHandle->CmdHasChanged=true;
}
void oszilloskop::m_setTriggerEnable(){
    switch (trigEnabled_dialVal){
        case Qt::Unchecked: {
            devHandle->CmdBuf[9]=(devHandle->CmdBuf[9] & 0xfd) + (unsigned char) (0x00 & 0x2); //disabled: remove enable bit (bit 1)
            break;
        }
        case Qt::Checked: {
            devHandle->CmdBuf[9]=(devHandle->CmdBuf[9] & 0xfd) + (unsigned char) (0x02 & 0x2); //enabled: set enable bit (bit 1)
            break;
        }
    }
    devHandle->CmdHasChanged=true;
    emit triggerStateChanged(trigEnabled_dialVal);
    m_checkNextSampleWaitTimerEnable();                     // disable when trigger enabled
}

void oszilloskop::m_setTimeBase(){
    // std::cout << "oszilloskop::m_setTimeBase()" << std::endl;
    switch (timeBase_dialVal){
    case 0: devHandle->CmdBuf[8]=(unsigned char) 0xc1; fs=     250;  memcpy(tbText,"  250 S/s", 10);break;
    case 1: devHandle->CmdBuf[8]=(unsigned char) 0xc2; fs=     625;  memcpy(tbText,"  625 S/s", 10);break;
    case 2: devHandle->CmdBuf[8]=(unsigned char) 0xe0; fs=    1250;  memcpy(tbText,"1.25 kS/s", 10);break;
    case 3: devHandle->CmdBuf[8]=(unsigned char) 0xe1; fs=    2500;  memcpy(tbText," 2.5 kS/s", 10);break;
    case 4: devHandle->CmdBuf[8]=(unsigned char) 0xe2; fs=    6250;  memcpy(tbText,"6.25 kS/s", 10);break;
    case 5: devHandle->CmdBuf[8]=(unsigned char) 0xf0; fs=   12500;  memcpy(tbText,"12.5 kS/s", 10);break;
    case 6: devHandle->CmdBuf[8]=(unsigned char) 0xf1; fs=   25000;  memcpy(tbText,"  25 kS/s", 10);break;
    case 7: devHandle->CmdBuf[8]=(unsigned char) 0xf2; fs=   62500;  memcpy(tbText,"62.5 kS/s", 10);break;
    case 8: devHandle->CmdBuf[8]=(unsigned char) 0xf8; fs=  125000;  memcpy(tbText," 125 kS/s", 10);break;
    case 9: devHandle->CmdBuf[8]=(unsigned char) 0xf9; fs=  250000;  memcpy(tbText," 250 kS/s", 10); break;
    case 10: devHandle->CmdBuf[8]=(unsigned char) 0xfa; fs=  625000; memcpy(tbText," 625 kS/s", 10); break;
    case 11: devHandle->CmdBuf[8]=(unsigned char) 0xfc; fs= 1250000; memcpy(tbText,"1.25 MS/s", 10); break;
    case 12: devHandle->CmdBuf[8]=(unsigned char) 0xfd; fs= 2500000; memcpy(tbText," 2.5 MS/s", 10); break;
    case 13: devHandle->CmdBuf[8]=(unsigned char) 0xfe; fs= 6250000; memcpy(tbText,"6.25 MS/s", 10); break;
    case 14: devHandle->CmdBuf[8]=(unsigned char) 0x80; fs=12500000; memcpy(tbText,"12.5 MS/s", 10); break;
    case 15: devHandle->CmdBuf[8]=(unsigned char) 0x40; fs=25000000; memcpy(tbText,"  25 MS/s", 10); break;
    }
    emit timeBaseKnobTextChanged(QString(tbText));          // to set the dial knob label
    devHandle->CmdHasChanged=true;


    if (fs < 4096){
        dt=1.0/fs;
        emit timeBaseChanged(dt, 4096.0*dt, QString("Time S")); //time axis scale, time axis title and trigger marker horizontal (time) position
    }
    else if (fs >= 4096 &&  fs <= 4096000){
        dt=1000.0/fs;
        emit timeBaseChanged(dt, 4096.0*dt, QString("Time mS"));
    }
    else if (fs >= 4096000){
        dt=1000000.0/fs;
        emit timeBaseChanged(dt, 4096.0*dt, QString("Time uS"));
    }
    m_checkNextSampleWaitTimerEnable();                     // enable at low sampling rate
}
void oszilloskop::m_checkNextSampleWaitTimerEnable(){
    if (fs < 1251 && trigEnabled_dialVal!=Qt::Checked){             // display counter at low samplig rates and only with trigger disabled
        emit waitLabelTimerOnOff(true, qFloor(4096.0/(qreal)fs));   // switch timer on
        if (!waitingForSampleSignalConneced){                       // make sure we connect only once
            connect(devHandle, &deviceHandler::waitingForSample, waitLabelTimer, &nextSampleWaitTimer::resetTimer);
            waitingForSampleSignalConneced=true;
        }
    }
    else {
        emit waitLabelTimerOnOff(false, 0);                         // switch timer off
        if (waitingForSampleSignalConneced){                        // make sure we disconnect only once
            disconnect(devHandle, &deviceHandler::waitingForSample, waitLabelTimer, &nextSampleWaitTimer::resetTimer);
            waitingForSampleSignalConneced=false;
        }
    }

}
void oszilloskop::m_startRunning(){
    devHandle->funcToRun = deviceHandler::sample_continuously;   // set what to do
    devHandle->runGranted=true;                                  // grant permission to run continuously (this is revoked to stop continuous run)
    devHandle->start();                                          // start the device handler thread
}
void oszilloskop::m_runOnce(){
    devHandle->funcToRun = deviceHandler::sample_once;           // set what to do
    devHandle->runGranted=true;                                  // grant permission to run continuously (this is revoked to stop continuous run)
    devHandle->start();                                          // start the device handler thread
}
void oszilloskop::m_startSimulating(){
    devHandle->funcToRun = deviceHandler::simulate_continuously; // set what to do
    devHandle->runGranted=true;                                  // grant permission to simulate continuously (this is revoked to stop continuous run)
    devHandle->start();                                          // start the device handler thread
}

void oszilloskop::m_calibrate(){

    #define execCalibration  1
    #if execCalibration == 1
    // unsigned char CmdBkp[10];
    if (! devHandle->initComplete){
        std::cout << "Device init needed first!" << std::endl;
        sleep (1);
        emit calibrationDone(oszilloskop::cal_fail);
        return;
    }
    if (devHandle ->scopeIsRunning){
        std::cout << "Device is busy! Hitting \"stop\" might help...!" << std::endl;
        sleep (1);
        emit calibrationDone(oszilloskop::cal_deviceBusy);
        return;
    }
    int settingBuf[20];     //save actual settings to Bkp
    settingBuf[1] = sensivCh1_dialVal,
    settingBuf[2] = sensivCh2_dialVal;                      // sensitivity dial value (int) for channel 1 & 2
    settingBuf[3] = offsCh1_dialVal;
    settingBuf[4] = offsCh2_dialVal;                        // offset for channel 1 & 2
    settingBuf[5] = couplingCh1_dialVal;
    settingBuf[6] = couplingCh2_dialVal;                    // coupling of channel 1 & 2, this holds on/off state (visibility) of each channel (needed to disable a channel trace)
    settingBuf[7] = trigLevel_dialVal;                      // trigger level dial value (int)
    settingBuf[8] = trigChanSel_dialVal;
    settingBuf[9] = trigEdge_dialVal;
    settingBuf[10]= trigEnabled_dialVal;                    // trigger channel, edge and on/off setting
    settingBuf[11]= timeBase_dialVal;                       // sampling time base and corresponding sampling frequency                    // time base text representation corresponding to sampling frequenca


    this->calibrationInPorgress = true;
    // this->CmdBuf[8]=0xfc;                                           //set Time Base to 1.25 MS/s
    this->timeBase_dialVal=11; m_setTimeBase();
    // this->CmdBuf[9]= 0x05;                                          //set trigger: off Ch2 fE

    // CmdBuf[3] = 0x10; CmdBuf[4] = 0x10;                             // set GND bit and clear DC bit (-> AC)
    devHandle->CmdBuf[3] = 0x00; devHandle->CmdBuf[4] = 0x00;          // clear GND bit and clear DC bit (-> AC)

    printf("0.01V\n"); fflush(stdout);
    // CmdBuf[3]=(CmdBuf[3] & 0xd1) + (unsigned char) (0x22 & 0x2e);   //set level bits (don't touch coupling bits) 0.01V
    this->sensivCh1_dialVal=5; this->m_setSensitivityCh1();
    // CmdBuf[4]=(CmdBuf[4] & 0xd1) + (unsigned char) (0x22 & 0x2e);   //set level bits (don't touch coupling bits) 0.01V
    this->sensivCh2_dialVal=5; this->m_setSensitivityCh2();
    this->m_findOffset(devHandle->calOffsCh1, devHandle->calOffsCh2);

    printf("0.03V\n"); fflush(stdout);
    // CmdBuf[3]=(CmdBuf[3] & 0xd1) + (unsigned char) (0x02 & 0x2e);   //0.03 V
    this->sensivCh1_dialVal=4; this->m_setSensitivityCh1();
    // CmdBuf[4]=(CmdBuf[4] & 0xd1) + (unsigned char) (0x02 & 0x2e);   //0.03 V
    this->sensivCh2_dialVal=4; this->m_setSensitivityCh2();
    this->m_findOffset(devHandle->calOffsCh1+1, devHandle->calOffsCh2+1);

    printf("0.1V\n"); fflush(stdout);
    // CmdBuf[3]=(CmdBuf[3] & 0xd1) + (unsigned char) (0x24 & 0x2e);   //0.1 V
    this->sensivCh1_dialVal=3; this->m_setSensitivityCh1();
    // CmdBuf[4]=(CmdBuf[4] & 0xd1) + (unsigned char) (0x24 & 0x2e);   //0.1 V
    this->sensivCh2_dialVal=3; this->m_setSensitivityCh2();
    this->m_findOffset(devHandle->calOffsCh1+2, devHandle->calOffsCh2+2);

    printf("0.03V\n"); fflush(stdout);
    // CmdBuf[3]=(CmdBuf[3] & 0xd1) + (unsigned char) (0x04 & 0x2e);   //0.3 V
    this->sensivCh1_dialVal=2; this->m_setSensitivityCh1();
    // CmdBuf[4]=(CmdBuf[4] & 0xd1) + (unsigned char) (0x04 & 0x2e);   //0.3 V
    this->sensivCh2_dialVal=2; this->m_setSensitivityCh2();
    this->m_findOffset(devHandle->calOffsCh1+3, devHandle->calOffsCh2+3);

    printf("1V\n"); fflush(stdout);
    // CmdBuf[3]=(CmdBuf[3] & 0xd1) + (unsigned char) (0x28 & 0x2e);   //1 V
    this->sensivCh1_dialVal=1; this->m_setSensitivityCh1();
    // CmdBuf[4]=(CmdBuf[4] & 0xd1) + (unsigned char) (0x28 & 0x2e);   //1 V
    this->sensivCh2_dialVal=1; this->m_setSensitivityCh2();
    this->m_findOffset(devHandle->calOffsCh1+4, devHandle->calOffsCh2+4);

    printf("3V\n"); fflush(stdout);
    // CmdBuf[3]=(CmdBuf[3] & 0xd1) + (unsigned char) (0x08 & 0x2e);   //3 V
    this->sensivCh1_dialVal=0; this->m_setSensitivityCh1();
    // CmdBuf[4]=(CmdBuf[4] & 0xd1) + (unsigned char) (0x08 & 0x2e);   //3 V
    this->sensivCh2_dialVal=0; this->m_setSensitivityCh2();
    this->m_findOffset(devHandle->calOffsCh1+5, devHandle->calOffsCh2+5);

#endif
    sleep (1);
    emit calibrationDone(oszilloskop::cal_success);
    this->calibrationInPorgress = false;
    /*for (int l=0; l<6; l++){
        printf("%x, ", *(calOffsCh1+l));
    }
    printf("    ");
    for (int l=0; l<6; l++){
        printf("%x, ", *(calOffsCh2+l));
    }
   printf("\n"); fflush(stdout);*/

    //restore actual setting from Bkp
    sensivCh1_dialVal = settingBuf[1];
    sensivCh2_dialVal = settingBuf[2];                    // sensitivity dial value (int) for channel 1 & 2
    offsCh1_dialVal = settingBuf[3];
    offsCh2_dialVal = settingBuf[4];                        // offset for channel 1 & 2
    couplingCh1_dialVal = settingBuf[5];
    couplingCh2_dialVal = settingBuf[6];                    // coupling of channel 1 & 2, this holds on/off state (visibility) of each channel (needed to disable a channel trace)
    trigLevel_dialVal = settingBuf[7];                      // trigger level dial value (int)
    trigChanSel_dialVal = settingBuf[8];
    trigEdge_dialVal = settingBuf[9];
    trigEnabled_dialVal = settingBuf[10];                    // trigger channel, edge and on/off setting
    timeBase_dialVal = settingBuf[11];                       // sampling time base and corresponding sampling frequency                    // time base text representation corresponding to sampling frequenca

    m_setSensitivityCh1();
    m_setSensitivityCh2();
    m_setOffsetCh1();
    m_setOffsetCh2();
    m_setCouplingCh1();
    m_setCouplingCh2();
    m_setTriggerLevel();
    m_setTriggerChannel();
    m_setTriggerEdge();
    m_setTriggerEnable();
    m_setTimeBase();
}


void oszilloskop::m_storeCalData(){
    QByteArray ch1QBA, ch2QBA;
    // printf("oszilloskop::m_storeCalData() threadId: %lu\n", pthread_self()); fflush(stdout);
    printf ("storing calibration Data...\n"); fflush(stdout);
    for (int i=0; i<6; i++){
        ch1QBA.append(*(devHandle->calOffsCh1+i));
        ch2QBA.append(*(devHandle->calOffsCh2+i));
    }

    calDataSettings->setValue("calOffsCh1", ch1QBA);
    calDataSettings->setValue("calOffsCh2", ch2QBA);
}

void oszilloskop::m_getCalDataFromStore(){
    QVariant ch1Var, ch2Var;
    QByteArray ch1QBA, ch2QBA;

    ch1Var=calDataSettings->value("calOffsCh1");
    ch2Var=calDataSettings->value("calOffsCh2");
    if (ch1Var.isNull() || ch2Var.isNull() ){
        printf("No valid calibration data found! Using defaults. You should consider calibrating...\n"); fflush(stdout);
        return;
    }
    ch1QBA = ch1Var.toByteArray();
    ch2QBA = ch2Var.toByteArray();
    const unsigned char* cc1 = (const unsigned char*)ch1QBA.constData();
    const unsigned char* cc2 = (const unsigned char*)ch2QBA.constData();
    for (int i=0; i<6; i++){
        *(devHandle->calOffsCh1+i)=(unsigned char)*(cc1+i);
        *(devHandle->calOffsCh2+i)=(unsigned char)*(cc2+i);
    }
    // printf("oszilloskop::m_getCalDataFromStore(): reading actual calibration data: ");
    /*for (int l=0; l<6; l++){
        printf("%x, ", *(devHandle->calOffsCh1+l));
    }
    printf("    ");
    for (int l=0; l<6; l++){
        printf("%x, ", *(devHandle->calOffsCh2+l));
    }
   printf("\n"); fflush(stdout);*/
   sleep (3);
   emit calibrationDataRestored();
}

void oszilloskop::m_findOffset(unsigned char *offsCh1, unsigned char *offsCh2){
    unsigned char _offsCh1=0x7f, _offsCh2=0x7f;
    signed char offsIncrCh1=1, offsIncrCh2=1;
    double meanMag1st[2], meanMag2nd[2];
    bool offsFoundCh1=false, offsFoundCh2=false;

    devHandle->CmdBuf[5]=_offsCh1, devHandle->CmdBuf[6]=_offsCh2;                               //set offs Ch1 and Ch2
    devHandle->funcToRun=deviceHandler::sample_once;
    devHandle->run();                                                                           //take 1st sample
    this->m_findMeanTwoChan(meanMag1st, meanMag1st+1, devHandle->rdBuf, 4096);


    if (meanMag1st[0] > 127.5)      {offsIncrCh1=1;}
    else if (meanMag1st[0] < 127.5)  {offsIncrCh1=-1;}
    if (meanMag1st[1] > 127.5)      {offsIncrCh2=1;}
    else if (meanMag1st[1] < 127.5)  {offsIncrCh2=-1;}

    _offsCh1=_offsCh1 + offsIncrCh1; _offsCh2=_offsCh2 + offsIncrCh2;                       //increase offset values

    meanMag1st[0]=std::abs(meanMag1st[0]-127.5);                                                  //calc abs(mean(Ch1))
    meanMag1st[1]=std::abs(meanMag1st[1]-127.5);                                                  //calc abs(mean(Ch2))

    while ( (!offsFoundCh1 || !offsFoundCh2) && (_offsCh1 > 0x50) && (_offsCh1 < 0xb4) && (_offsCh2 > 0x50) && (_offsCh2 < 0xb4)){ //make sure both offsets stay between 0x50 ... 0xb4
        devHandle->CmdBuf[5]=_offsCh1, devHandle->CmdBuf[6]=_offsCh2;                                                   //set offs Ch1 and Ch2
        devHandle->funcToRun=deviceHandler::sample_once;
        devHandle->run();                                                                           //take 2nd sample


        this->m_findMeanTwoChan(meanMag2nd, meanMag2nd+1, devHandle->rdBuf, 4096);
        //printf("_findOffset(): meanMag2nd %f meanMag2nd+1 %f\n", *meanMag2nd, *(meanMag2nd+1)); fflush(stdout);

        meanMag2nd[0]=std::abs(meanMag2nd[0]-127.5);                                                  //calc abs(mean(Ch1))
        meanMag2nd[1]=std::abs(meanMag2nd[1]-127.5);                                                  //calc abs(mean(Ch2))
        printf("_findOffset(): meanMag2nd %f meanMag2nd+1 %f\n", *meanMag2nd, *(meanMag2nd+1)); fflush(stdout);

        if ( (meanMag2nd[0] > meanMag1st[0]) && !offsFoundCh1){ //compare meanMag and meanMagXXprevAttmpt, if it grew (after incr offs): last offs value was the best one
            offsFoundCh1 = true;            //ready
            *offsCh1=_offsCh1 - offsIncrCh1;  //store offsCh1=offsCh1 - offsIncrCh1;
        }
        else if ( (meanMag2nd[0] < meanMag1st[0]) && !offsFoundCh1) {
            _offsCh1=_offsCh1 + offsIncrCh1;
        }

        if ( (meanMag2nd[1] > meanMag1st[1]) && !offsFoundCh2){ //compare meanMag and meanMagXXprevAttmpt, if it grew (after incr offs): last offs value was the best one
            offsFoundCh2 = true;            //ready
            *offsCh1=_offsCh2 - offsIncrCh2;  //store offsCh1=offsCh1 - offsIncrCh1;
        }
        else if ( (meanMag2nd[1] < meanMag1st[1]) && !offsFoundCh2) {
            _offsCh2=_offsCh2 + offsIncrCh2;
        }
        meanMag1st[0]=meanMag2nd[0]; meanMag1st[1]=meanMag2nd[1];                               //store actual values in old bins...
        }
    *offsCh1 = _offsCh1; *offsCh2 = _offsCh2;
    printf("optimal offset found\n");  fflush(stdout);
}
void oszilloskop::m_findMeanTwoChan(double *meanCh1, double *meanCh2, unsigned char *buf, int len){

    *meanCh1=0, *meanCh2=0;
    for(int l=0; l<len; l++){
        *meanCh1=*meanCh1 + (double) *(buf+2*l+1);
        *meanCh2=*meanCh2 + (double) *(buf+2*l);
    }
//    printf("fMTC: %d\n", *(buf+8));
    *meanCh1=*meanCh1/len;
    *meanCh2=*meanCh2/len;
    //printf("findMean(): meanCh1: %f\n", *meanCh1);
}

// void testfunc(){
//     std::cout << "oszilloskop::testfunc()" << std::endl;
// }
