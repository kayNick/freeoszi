#ifndef NEXTSAMPLEWAITTIMER_H
#define NEXTSAMPLEWAITTIMER_H
#include <QObject>
#include <QTimer>
#include <QLabel>
// ----------------- class nextSampleWaitTimer ------------------------------

class nextSampleWaitTimer : public QObject{
    Q_OBJECT
public:
    QLabel *waitLongForSampleLablel = nullptr;              // pointer to label to show wait message

private:
    QTimer *timer;
    int secondsToWait=0;                                    // how many seconds to wait for next sample
    int t=0;
    bool doCount=false;
public:
    nextSampleWaitTimer(QLabel *waitDisplayLabel = nullptr) : waitLongForSampleLablel(waitDisplayLabel), timer(new QTimer(this)) {
        QObject::connect(timer, &QTimer::timeout, this, &nextSampleWaitTimer::waitLongForSampleCountDown);
    }

    ~nextSampleWaitTimer(){
        delete timer;
    }

    void startTimer(int seconds){                                  // exec this to show wait label and start count down
        if (waitLongForSampleLablel == nullptr) {
            printf ("Error: QLabel *waitLongForSampleLablel has not been set!\n"); fflush(stdout);
            return;
        }
        else {
            secondsToWait=seconds;
            t=secondsToWait;
            waitLongForSampleLablel->setText(QString("<font color='brown'>Next sample expected in ").append(QString::number(t)).append(QString(" sec</font>")));
            if ( ! timer->isActive() ) {
                printf("starting timer...\n"); fflush(stdout);
                timer->start(500);}                                // wait 1000 ms between label updates
        }
    }

    void stopTimer(){
        if (timer->isActive() ) {
             printf("stoping timer...\n"); fflush(stdout);
            timer->stop();
            waitLongForSampleLablel->setText("");               // clear waiting label
        }
    }

public slots:
    void waitLabelTimerStartStop(bool state, int time){
        if (state == true) startTimer(time);
        if (state == false) stopTimer();
    }
    void resetTimer(){
        printf("resetting counter...%d\n", t); fflush(stdout);
        t=secondsToWait;
        doCount=false;
    }


    void waitLongForSampleCountDown(){                          // timer executes this every second

        if (t == 0){
            waitLongForSampleLablel->setText("");               // clear waiting label when timer elapsed
        }

        else {
            waitLongForSampleLablel->setText(QString("<font color='brown'>Next sample expected in ").append(QString::number(t)).append(QString(" sec</font>")));
            // printf("nextSampleWaitTimer::waitLongForSampleCountDown() %d\n", t); fflush(stdout);
            doCount = !doCount;
            if (doCount) t--;
        }
    }

};

#endif // NEXTSAMPLEWAITTIMER_H
