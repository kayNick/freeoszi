#include "scopeView.h"

scopeView::scopeView(oszilloskop *oszi,  deviceHandler *dH):
    dt(oszi->dt), devHandle(dH), m_chart(new QChart), tView(new QChartView(m_chart)),
    ch1_series(new QLineSeries), ch2_series(new QLineSeries), axisYCh1(new QValueAxis), axisYCh2(new QValueAxis),
    axisTime(new QValueAxis), ch1QVec(new QVector<QPointF>), ch2QVec(new QVector<QPointF>),
    tE(new trigEngine(m_chart, ch1_series, ch2_series, &dt))
{
    maxCh1=4.0 * oszi->sensitivityCh1;  maxCh2=4.0 * oszi->sensitivityCh2;

    tView->setMinimumSize(600, 450);
    tView->setContentsMargins(0,0,0,0);
    m_chart->setWindowFrameMargins(0,0,0,0);
    m_chart->setContentsMargins(0,0,0,0);

    m_chart->addSeries(ch1_series);
    m_chart->addSeries(ch2_series);


    axisYCh1->setRange(-maxCh1, maxCh1);
    axisYCh1->setTickCount(9);
    axisYCh1->setLabelFormat("%3.2f");
    axisYCh1->setTitleText("");
    //axisYCh1->setGridLineColor(Qt::blue);
    axisYCh1->setLabelsColor(Qt::blue);

    axisYCh2->setRange(-maxCh2, maxCh2);
    axisYCh2->setTickCount(9);
    axisYCh2->setLabelFormat("%3.2f");
    axisYCh2->setLabelsColor(Qt::darkGreen);

    axisTime->setRange(0, 4096*dt);
    axisTime->setTickCount(9);
    axisTime->setLabelFormat("%g");
    //axisX->setLabelFormat(" ");
    axisTime->setTitleText("Time ms");

    m_chart->addAxis(axisTime, Qt::AlignBottom);
    ch1_series->attachAxis(axisTime);
    ch2_series->attachAxis(axisTime);

    m_chart->addAxis(axisYCh1, Qt::AlignLeft);
    ch1_series->attachAxis(axisYCh1);
    m_chart->addAxis(axisYCh2, Qt::AlignRight);
    ch2_series->attachAxis(axisYCh2);

    m_chart->legend()->hide();
    m_chart->setTitle(QString("Scope"));

    memcpy(tmp, devHandle->rdBuf, 8192);
    memcpy(sampleDataBuffer, tmp+8129, 63); memcpy(sampleDataBuffer+63, tmp, 8129); //some kind of weird glitch in data handling
    for (int l=0; l<4096; l++){
        //ch1QVec->append(QPointF((qreal)l*dt, *maxCh1*qSin(2.*3.14*2*l/4096)));
        //ch2QVec->append(QPointF((qreal)l*dt, 0.6**maxCh2*qCos(2.*3.14*3*l/4096+6*28*0.5)+2.0*(qreal)l/4096-1.0 ));
        //ch1QVec->append(QPointF( (qreal)l*dt, maxCh1 * ( ((qreal) *(sampleDataBuffer+ 2*l  ))/127.5-1.0)  )  );
        //ch2QVec->append(QPointF( (qreal)l*dt, maxCh2 * ( ((qreal) *(sampleDataBuffer+ 2*l+1))/127.5-1.0)  )  );
        ch1QVec->append(QPointF( (qreal)l*dt,   ( ((qreal) *(sampleDataBuffer+ 2*l  ))/127.5-1.0)  )  );
        ch2QVec->append(QPointF( (qreal)l*dt,   ( ((qreal) *(sampleDataBuffer+ 2*l+1))/127.5-1.0)  )  );

    }
    ch1_series->replace(*ch1QVec);
    ch2_series->replace(*ch2QVec);
    if (oszi->couplingCh1_dialVal == 4) ch1_series->setVisible(false);
    if (oszi->couplingCh2_dialVal == 4) ch2_series->setVisible(false);

    author = new QGraphicsSimpleTextItem("by K. Nick\ndr.k.nick@gmx.de", m_chart);
    //author->setText("by K. Nick\ndr.k.nick@gmx.de");
    //author->setPos(0, 10);


    ch1AxisLbl = new QGraphicsSimpleTextItem("Ch 1", m_chart);
    ch1AxisLbl->setPen(QPen(Qt::blue));
    ch2AxisLbl = new QGraphicsSimpleTextItem("Ch 2", m_chart);
    ch2AxisLbl->setPen(QPen(Qt::darkGreen));

    ovL = new QGraphicsSimpleTextItem(m_chart);
    ovL->setText("OL");
    ovL->setPos(150, 20);
    ovL->setBrush(QBrush(Qt::red));
    ovL->hide();
    connect(m_chart, SIGNAL(plotAreaChanged(const QRectF &)), this, SLOT(updateOnResize(const QRectF &)));
    if (oszi->trigEnabled_dialVal == Qt::Checked) tE->enable();         // enable trigger marker if triggrting is enabled on init
    tE->setChan(oszi->trigChanSel_dialVal);                             // set trigger marker to correct trigger channel on init
    //tE->setChan(oszi->trigEdge_dialVal);                                // set trigger marker to correct trigger edge on init
    tE->setLevel(oszi->trigLevel);                                      // set trigger marker to correct trigger level on init
}

scopeView::~scopeView(){
    delete m_chart;
    delete tE;
}

QChartView * scopeView::getChartView(){
    return tView;
}

void scopeView::updateSeries(){             // this takes row sample data from devHandle and processes it for time based display
    if (devHandle->rdBufMutex->try_lock()){
        memcpy(tmp, devHandle->rdBuf, 8192);
        devHandle->rdBufMutex->unlock();
    }
    memcpy(sampleDataBuffer, tmp+8129, 63); memcpy(sampleDataBuffer+63, tmp, 8129); //some kind of weird glitch in data handling requires putting last 63 bytes at the beginning of the data buffer
    for (int l=0; l<4096; l++){

        ch1QVec->replace(l, QPointF( (qreal)l*dt, maxCh1 * ( ((qreal) *(sampleDataBuffer+ 2*l  ))/127.5-1.0)  )  );
        ch2QVec->replace(l, QPointF( (qreal)l*dt, maxCh2 * ( ((qreal) *(sampleDataBuffer+ 2*l+1))/127.5-1.0)  )  );
        if ( *(sampleDataBuffer+ 2*l) % 255 == 0 || *(sampleDataBuffer+ 2*l+1) % 255 == 0 ){this->showOvL();}
    }
    ch1_series->replace(*ch1QVec);
    ch2_series->replace(*ch2QVec);
    emit  updateMeanRMS(sampleDataBuffer, 4096);
}
void scopeView::setTimeBase(qreal dt, qreal axisTimeMax, QString axisTimeText){
    axisTime->setRange(0, axisTimeMax);
    axisTime->setTitleText(axisTimeText);
    this->dt=dt;
    tE->setTimeBase(dt);
}
void scopeView::showOvL(){
    if (! ovL->isVisible()){
        ovL->show();
        QTimer::singleShot(5000, this, SLOT(hideOvL()));
    }
}
void scopeView::hideOvL(){
    ovL->hide();
}
void scopeView::updateOnResize(const QRectF &rect){
    //printf("scopeView::updateOnResize\n"); fflush(stdout);
    author->setPos(m_chart->geometry().width()-200,10);
    ch1AxisLbl->setPos(m_chart->geometry().x()+40, 20);
    ch2AxisLbl->setPos(m_chart->geometry().width()-65, 20);
}
void scopeView::setYRangeCh1(qreal sensitivityCh1){
    maxCh1=4*sensitivityCh1;
    axisYCh1->setRange(-maxCh1, maxCh1);
}
void scopeView::setYRangeCh2(qreal sensitivityCh2){
    maxCh2=4*sensitivityCh2;
    axisYCh2->setRange(-maxCh2, maxCh2);
}
void scopeView::setVisibleCh1(bool visibleCh1){
    ch1_series->setVisible(visibleCh1);
}
void scopeView::setVisibleCh2(bool visibleCh2){
    ch2_series->setVisible(visibleCh2);
}
void scopeView::setTriggerState(int triggerState){
    //printf("scopeView::setTriggerstate: %d\n", triggerState); fflush(stdout);
    switch (triggerState){
        case Qt::Unchecked:{tE->disable(); break;}
        case Qt::Checked:  {tE->enable(); break;}
    }
}
void scopeView::setTriggerLevel(qreal triggerlevel){
    tE->setLevel(triggerlevel);
}
void scopeView::setTriggerChannel(int triggerchannel){
    tE->setChan(triggerchannel);
}
