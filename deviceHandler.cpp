#include "deviceHandler.h"
#include <cmath>


deviceHandler::deviceHandler() : rdBufMutex(new QMutex)
{
    memcpy(CmdBuf, "\x0e\x80\x07\x05\x05\x74\x71\x7f\xfa\x00", 10); // init CmdBuf with sane data (to match init knob settings)


    for(int k=0; k<8192; k=k+2){												//read 8 chunks of data
        *(rdBuf+(k+8129)%8192)   = ((unsigned char)  1/12.5*127.5* cos(2*3.14*11*k/8192)+127.5 ); // init rdBuf with some sensible data
        *(rdBuf+(k+1+8129)%8192) = ((unsigned char)  1/9.5*127.5* sin(2*3.14*28*k/8192)+127.5 );  // to show on startup
     }
    // printf ("deviceHandler::deviceHandler() size of genSampleBuffer: %lu\n", sizeof (genSampleBuffer)); fflush(stdout);
}

deviceHandler::~deviceHandler()
{
    //if (scopeIsRunning){
        printf("Killing thread...!\n"); fflush(stdout);
        //runGranted = false;
        //QThread::msleep(1250);
    //}
    if (initComplete){
        printf("Device init needed first!\n"); fflush(stdout);
        stopdev(devhandle);
    }

}

void deviceHandler::run(){
    switch (funcToRun){
    case deviceHandler::initialze: initLibUSB();  initDevice(); break;
    case deviceHandler::run_calibration:       ; break;
    // case deviceHandler::run_testFunc:  (this->*funcPointer)();     ; break;
    case deviceHandler::sample_continuously:   runCont(); break;
    case deviceHandler::sample_once:           runOnce(); break;
    case deviceHandler::simulate_continuously: simulate(); break;
    case deviceHandler::reset_device:          resetDev(); break;
    }
}

void deviceHandler::stopdev(libusb_device_handle *devhandle){

    write2dev(devhandle, (unsigned char*)"\x09", 1);

    write2dev(devhandle, (unsigned char*)"\x0b", 1);

    write2dev(devhandle, (unsigned char*)"\x04", 1);

    write2dev(devhandle, genSampleBuffer , sizeof(genSampleBuffer));	//512 bytes

    write2dev(devhandle, (unsigned char*)"\x0e\x05\x04\x80\x00\x03\x0f", 7);

    write2dev(devhandle, (unsigned char*)"\x06", 1);

    write2dev(devhandle, (unsigned char*)"\x14", 1);

}

void deviceHandler::initLibUSB(){
    int ret;
    ret = libusb_init (&context);										// init libusb & create context
    if (ret < 0){
        perror("libusb_init");
    }
    num_of_devices_found = libusb_get_device_list(context, &devs);		// create device list

    corr_device = _find_correct_device(devs, num_of_devices_found);		// find correct device in list and incr its ref count

    libusb_free_device_list(devs, 1); 									// free all devs short of correct one (that with incr ref count)

    if (corr_device == NULL){
        fprintf(stderr, "deviceHandler::deviceHandler(): No fitting device found!\n");
        return;
   }

    libusb_open(corr_device, &devhandle);								// open device
    if (devhandle == NULL) {
        perror("ibusb_open_device_with_vid_pid failed");
    }
    else { printf("Device open succeded...\n");}
                                                                        //detach kernel driver if necessary
    if(libusb_kernel_driver_active(devhandle, 0) == 1) { 				//find out if kernel driver is attached
        printf("Kernel Driver Active\n");
        if(libusb_detach_kernel_driver(devhandle, 0) == 0) 				//detach it
            printf("Kernel Driver Detached!\n");
    }
    else { printf("No Kernel Driver Active found...\n");}

    ret = libusb_get_string_descriptor_ascii (devhandle, desc.iManufacturer, confStrBuf, 100);
    confStrBuf[ret]=0;
    printf("Manufacturer: %s\n", confStrBuf);
    memset(confStrBuf, 0, 100);
    ret = libusb_get_string_descriptor_ascii (devhandle, desc.iProduct, confStrBuf, 100);
    confStrBuf[ret]=0;
    printf("Product: %s\n", confStrBuf);
    memset(confStrBuf, 0, 100);
    ret = libusb_get_string_descriptor_ascii (devhandle, desc.iSerialNumber, confStrBuf, 100);
    confStrBuf[ret]=0;
    printf("Serial: %s\n", confStrBuf);


    //claim interface
    ret = libusb_claim_interface(devhandle, 0); 						//claim interface 0 (the first) of device (mine had jsut 1)
    if(ret < 0) {
        printf("Cannot Claim Interface\n");
}
    else{printf("Interface claimed... \n");}
}

int deviceHandler::initDevice(){

    unsigned char rBuffer[64];

    int k, ret, res=-1;
    if (devhandle == NULL){
        fprintf(stderr, "deviceHandler::initDevice(): devhandle should not be NULL!\n");
        return -2;
    }

    //----------------------- set right config
    //  int libusb_control_transfer (libusb_device_handle *dev_handle, uint8_t bmRequestType, uint8_t bRequest, uint16_t wValue, uint16_t  	wIndex, unsigned char *data, uint16_t wLength, unsigned int timeout);
    libusb_control_transfer(devhandle, 0, 9, 1, 0, NULL, 0, 500);
    sleep(1);
    //-----------------------


    write2dev(devhandle, (unsigned char*)"\x08" , 1);

    for(k=0; k<13; k++){												// upload fw in several chunks
        write2dev(devhandle, fw1Str+k*4096 , 4096); // 4096 bytes
    }
    write2dev(devhandle, fw1Str+13*4096, 1664); // 1664 bytes

    write2dev(devhandle, (unsigned char*)"\x0e\x80\x07\x29\x29\x75\x6e\x7f\x00\x00", 10);
    write2dev(devhandle, (unsigned char*)"\x0e\x80\x07\x29\x29\x75\x6e\x7f\x00\x00", 10);
    write2dev(devhandle, (unsigned char*)"\x0e\x80\x07\x29\x29\x75\x6e\x7f\xf8\x00", 10);
    write2dev(devhandle, (unsigned char*)"\x0e\x80\x07\x29\x29\x75\x6e\x7f\xf8\x00", 10);
    write2dev(devhandle, (unsigned char*)"\x0e\x80\x07\x29\x29\x75\x6e\x7f\xf8\x00", 10);
    write2dev(devhandle, (unsigned char*)"\x0e\x80\x07\x29\x29\x75\x6e\x7f\xf8\x00", 10);


    // ---------  this is probably optional for the generator

    write2dev(devhandle, (unsigned char*)"\x04" , 1);

    write2dev(devhandle, genSampleBuffer , sizeof(genSampleBuffer));						// fw part 2

    write2dev(devhandle, (unsigned char*)"\x0e\x05\x04\x80\x00\x13\x0f", 7);			//after this sequence is sent blue light comes on

    write2dev(devhandle, (unsigned char*)"\x06" , 1);

    write2dev(devhandle, (unsigned char*)"\x0f" , 1);


    printf("vor readfrdev...\n"); fflush(stdout);
    ret = readfrdev(devhandle, rBuffer, 64);					// hier kommt ein interrupt in
    printf("nach readfrdev...\n"); fflush(stdout);
    printf("devinit(): bytes read from interr: %d\n", ret);      fflush(stdout);
    printf("%c%c%c%c - ",rBuffer[0],rBuffer[1],rBuffer[2],rBuffer[3]);    fflush(stdout);

    for(k=0; k<ret; k++){
        printf("%x ", rBuffer[k]);
    }
    printf("\n"); fflush(stdout);
    initComplete=true;


// ---------------- hier ist device init vorbei ------------------------------------
    printf("device init complete\n"); fflush(stdout);
    return res;
}

libusb_device* deviceHandler::_find_correct_device(libusb_device **devs, size_t num_of_devices_found){
    libusb_device *corr_device=NULL;
    struct libusb_device_descriptor desc;
    uint16_t vendor_id = 0x10cf, product_id = 0x2501;
    int i, ret;

    // if (num_of_devices_found < 0){
    //     fprintf(stderr, "Error in getting device list\n");
    //    return NULL;
    //}

    printf("%d devices have been found\n", (int)num_of_devices_found); fflush(stdout);
                                                                        //find correct device in list
    for(i=0; i<(int)num_of_devices_found; i++){
        ret = libusb_get_device_descriptor(devs[i], &desc);
        if (ret < 0){
            fprintf(stderr, "error in getting device descriptor\n");
        return NULL;
        }
        printf("Vendor ID and Product ID %04x:%04x", desc.idVendor, desc.idProduct);
        if (desc.idVendor == vendor_id && desc.idProduct == product_id){
            printf(" <-- This is the one... Index is %d.", (int) i);
            corr_device = libusb_ref_device (devs[i]) ; 				// increment reference to correct device (prevent libusb_free_device_list() from destroying it...)
        }
        printf("\n");
    }
    printf("\n");

    //printf("find_correct_device(): %d %d \n", corr_device, devs[8]);
    fflush(stdout);


    if (corr_device == NULL){
        fprintf(stderr, "No fitting device found!\n");
        return NULL;
    }

    return corr_device;
}

void deviceHandler::runOnce(){
    int  ret;
    unsigned char intRdBuf[64];
    if (!initComplete){
        printf("Device init needed first!\n"); fflush(stdout);
        return;
    }
    if (scopeIsRunning){
        printf("Device init needed first!\n"); fflush(stdout);
        return;
    }
    scopeIsRunning = true;

    //printf("_runOnce() started...\n"); fflush(stdout);
    write2dev(devhandle, CmdBuf, 10);
    write2dev(devhandle, CmdBuf, 10);
    write2dev(devhandle, CmdBuf, 10);
    write2dev(devhandle, (unsigned char*)"\x09" , 1);
    write2dev(devhandle, (unsigned char*)"\x0b" , 1);


    memset(intRdBuf, 0, 64);

    for(int k=0; k<20000; k++){											//wait for data ready
        ret=readfrdev(devhandle, intRdBuf, 1);

        if (intRdBuf[0] == 68){                                         //0x44, this indicates: samplig done, data ready
            break;
        }
        QThread::usleep(1000);
    }

    write2dev(devhandle, (unsigned char*)"\x0a" , 1);
    rdBufMutex->lock();                                                 //protect access to rdBuf
    memset(rdBuf, 0, 8192);
    for(int k=0; k<8; k++){												//read 8 chunks of data
        ret=readfrdev(devhandle, rdBuf+k*1024, 1024);
    }
    rdBufMutex->unlock();                                               //lift access protection to rdBuf

    emit sampleDataReady();
    write2dev(devhandle, CmdBuf, 10);
    write2dev(devhandle, (unsigned char*)"\x09" , 1);
    write2dev(devhandle, (unsigned char*)"\x0b" , 1);
    readfrdev(devhandle, intRdBuf, 64);									//interrupt packet in

    ret = libusb_control_transfer(devhandle, 2, 1, 0, 129, NULL, 0, 500); // CLEAR FEATURE REQUEST control transfer
    scopeIsRunning = false;
}

void deviceHandler::simulate(){
    time_t t;
    int df;
    unsigned char *rnd = (unsigned char *) &df;
    if (scopeIsRunning){
        printf("An other worker is runnung!\n"); fflush(stdout);
        return;
    }

    /* Intializes random number generator */
       srand((unsigned) time(&t));
    scopeIsRunning = true;
    while (runGranted){

        rdBufMutex->lock();                                                 //protect access to rdBuf
        memset(rdBuf, 0, 8192);
        for(int k=0; k<8192; k=k+2){												//read 8 chunks of data
            df=rand();
            //memcpy(rdBuf+k, (unsigned char*)&df, 1);
            *(rdBuf+(k+8129)%8192)   = (unsigned char)  nearbyint(1.0/1.2*127.5* cos(2*3.14*11*k/8192)+127.5 + ((double)*(rnd+1)-127.5)/50.0 );
            *(rdBuf+(k+1+8129)%8192) = (unsigned char)  nearbyint(0.5*127.5* sin(2*3.14*28*k/8192)+127.5  + ((double)*(rnd+2)-127.5)/50.0);
            //*(rdBuf+(k+8129)%8192)   = (unsigned char) 127;
            //*(rdBuf+(k+1+8129)%8192) = (unsigned char) 128;


            //memcpy(sampleDataBuffer, tmp+8129, 63); memcpy(sampleDataBuffer+63, tmp, 8129);
            //memcpy(rdBuf+k, ((unsigned char*)&df)+2, 1);
           //printf("bytes read: %d, shoud be 1024\n",ret); fflush(stdout);
           }
        //rdBuf[2345]=0;
           rdBufMutex->unlock();                                               //lift access protection to rdBuf

           emit sampleDataReady();
           QThread::msleep(50);
        }
    scopeIsRunning = false;
}

void deviceHandler::runCont(){

    unsigned char intRdBuf[64];                                                 // interrupt read buffer
    if (!initComplete){
        printf("Device init needed first!\n"); fflush(stdout);
        return;
    }
    if (scopeIsRunning){
        printf("An other worker is runnung!\n"); fflush(stdout);
        return;
    }
    scopeIsRunning = true;                                                      // set marker indicating that oscilloscope is running in continuous mode

    write2dev(devhandle, CmdBuf, 10);
    write2dev(devhandle, (unsigned char*)"\x09" , 1);
    write2dev(devhandle, (unsigned char*)"\x0b" , 1);
    while (runGranted){

            memset(intRdBuf, 0, 64);                                            // zero out interrupt read buffer
            emit waitingForSample();                                            // indicate start of sample await periode (for indicator of long awaiting time (label at time base dia knob)
            for(int k=0; k<20000; k++){                                         // wait loop for sample data to become ready
                readfrdev(devhandle, intRdBuf, 64);

                if (CmdHasChanged){                                             // check if scope controll command has been changen and needs to be sent to device
                    libusb_control_transfer(devhandle, 2, 1, 0, 129, NULL, 0, 500);   // CLEAR FEATURE REQUEST control transfer, f. e. needed when stuck waiting for trigger condition
                    write2dev(devhandle, CmdBuf, 10);
                    write2dev(devhandle, (unsigned char*)"\x09" , 1);
                    write2dev(devhandle, (unsigned char*)"\x0b" , 1);
                    readfrdev(devhandle, intRdBuf, 64);                         // interrupt packet in
                    CmdHasChanged=false;                                        // clear marker for changed scope control command
                    QThread::usleep(200);
                }
                if (genCmdHasChanged){
                    genCmdSend();
                    genCmdHasChanged=false;
                }

                if (intRdBuf[0] == 0x44) break;                                 // this indicates: samplig done, data ready -> get out of for loop

                QThread::usleep(4000);
                if (k== 20000-1){
                    printf("deviceHandler::_rundCont(): for timed out...\n"); fflush(stdout);
                }
            }

            write2dev(devhandle, (unsigned char*)"\x0a" , 1);                   // as soon as sample data becomes ready send command to fetch it
            rdBufMutex->lock();                                                 // protect access to rdBuf
            memset(rdBuf, 0, 8192);
            for(int k=0; k<8; k++){												// read 8 chunks of data
                readfrdev(devhandle, rdBuf+k*1024, 1024);
                QThread::usleep(200);
            }
            rdBufMutex->unlock();                                               // lift access protection to rdBuf

            emit sampleDataReady();                                             // indicate that sample data is ready

            write2dev(devhandle, (unsigned char*)"\x09" , 1);
            write2dev(devhandle, (unsigned char*)"\x0b" , 1);
            readfrdev(devhandle, intRdBuf, 64);                                 // interrupt packet in
        }
    scopeIsRunning = false;
}

int deviceHandler::readfrdev(libusb_device_handle *devhandle, unsigned char *rBuf, int len){
    int devBytesTransferred, ret;
    ret = libusb_interrupt_transfer ( devhandle, 129, rBuf, len,  &devBytesTransferred, 500); // 128 and bigger is in
    if (ret < 0 ){
        printf("readfrdev(): Error when reading from usb device. Error code is: %d (-7 need not be bad)\n", ret);
        printf ("error was: %s\n", libusb_error_name(ret)); fflush(stdout);
        printf ("error desc: %s\n", libusb_strerror(static_cast<enum libusb_error>(ret)));
        //fprintf(stderr, "readfrdev(): Error when reading from usb device. Error code is: %d\n", ret); fflush(stderr);fflush(stdout);
        //qDebug()<< "Error when reading from device. Err code is: " << ret;
    }
    return devBytesTransferred;
}

int deviceHandler::write2dev(libusb_device_handle *devhandle, unsigned char *wBuf, int len){
    int ret, devBytesTransferred;

    ret = libusb_interrupt_transfer ( devhandle, 1, wBuf, len,  &devBytesTransferred, 500);  //127 an smaler is out
    if (ret < 0){
        //fprintf(stderr, "error when writing...%s\n", libusb_strerror (ret));
         qDebug()<< "Error when writing to device. Err code is: " << ret;
    }
    return devBytesTransferred;
}

void deviceHandler::resetDev(){
    int ret;
    ret = libusb_reset_device (devhandle);
    printf ("libusb_reset_device(): ret= %d\n", ret); fflush(stdout);
}


void deviceHandler::printCmdBuf(){
    int k;
    //printf("this is printwBuf()\n"); fflush(stdout);
    for (k=0; k<10; k++){
        printf("%x ", CmdBuf[k]);
    }
    printf("\n");fflush(stdout);
}
// --------- generator stuff here --------------------------

void deviceHandler::genCmdSend(){
    if (!initComplete){
        printf("Device init needed first!\n"); fflush(stdout);
        return;
    }

    //printgenCmdBuf();

    write2dev(devhandle, genCmdBuf, 7);                                     // send generator control command to device
    QThread::msleep(1);
    write2dev(devhandle, (unsigned char*)"\x04" , 1);
    QThread::msleep(1);
    write2dev(devhandle, genSampleBuffer , sizeof(genSampleBuffer));        // send signal sample data to device
    QThread::msleep(1);
    write2dev(devhandle, genFreqCmdBuf, 22);                                // send generator frequency to device
    QThread::msleep(1);
    write2dev(devhandle, (unsigned char*)"\x06" , 1);
    QThread::msleep(5);
}


void deviceHandler::printgenCmdBuf(){
    printf("### deviceHandler::printgenCmdBuf()\n");fflush(stdout);
    printf("    genCmdBuf: ");
    for (int k=0; k<7; k++){
        printf("%x ", genCmdBuf[k]);
    }
    printf("\n");
    printf("    genFreqCmdBuf: ");
    for (int k=0; k<22; k++){
        printf("%x ", genFreqCmdBuf[k]);
    }
    printf("\n");fflush(stdout);
}
