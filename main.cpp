#include "scopeWin.h"
#include "genWin.h"
#include "deviceHandler.h"

#include <QApplication>
#include <QPoint>
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    deviceHandler dH;           // create just one deviceHandler object
    fftEngine fftE;
    scopeWin w(&dH, &fftE);     // pass deviceHandler object to scope gui

    w.show();

    genWin g(&dH);              // pass deviceHandler object to generator gui
    g.show();
    w.otherWin=&g;
    g.otherWin=&w;
    return a.exec();
}
