#ifndef FFTENGINE_H
#define FFTENGINE_H
extern "C" {
#include <fftw3.h>
}

class fftEngine // fftw does not like being initialized and destroyed repeatedly during one app execution
                // it starts producing garbage when you do that
                // fftEngine class overcomes this by being inited once in main.cpp and then passed along to specView...
{
public:
    fftEngine();
    ~fftEngine();
    void exec();

    fftw_plan planCh1, planCh2;
    fftw_complex *inCh1, *outCh1, *inCh2, *outCh2;

};

#endif // FFTENGINE_H
