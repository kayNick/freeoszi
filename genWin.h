#pragma once

#include <QWidget>
#include <QLabel>
#include <QRadioButton>
#include <QButtonGroup>
#include <QGridLayout>
#include "genView.h"
#include "deviceHandler.h"
#include "genOperator.h"

class genWin : public QWidget
{
    Q_OBJECT

public:
    explicit genWin(deviceHandler *dH, QWidget *parent=nullptr);
    ~genWin();

public slots:
    void rcvDialFreqLevel(int);
    void rcvDialAmplLevel(int);
    void rcvDialOffsLevel(int);
    void rcvDialByte5(int);
    void rcvDialByte6(int);
    void rcvsigFormRBG(int);
    void updateSigView();


signals:
    void freqChanged(QString);
    void amplChanged(QString);
    void offsChanged(QString);

public:
    deviceHandler *devHandle=nullptr;
    genOperator *genOp=nullptr;

private:
    genView *geV=nullptr;
    QLabel *sigFormLbls=nullptr;
    QRadioButton *sigFormRBs = nullptr;
    QGridLayout *sigFormGLayout = nullptr;
    QButtonGroup *sigFormRBG = nullptr;
    float freqComponent[3];
    QLabel *amplCoLbl, *amplFnLbl, *amplSwLbl, *offsLbl;

    void calcAmplitudeDisplay();
    void placeFreqSliders();
    void placeFreqSweepEdits();
public:
    QWidget *otherWin; //just to be able to call close()

protected:
    void closeEvent(QCloseEvent *) override;

};
