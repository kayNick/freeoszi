#include <QApplication>
#include <QThread>
#include <QtWidgets/QDial>
#include <QtWidgets/QLabel>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QGraphicsTextItem>
#include <QGraphicsLineItem>
#include <QSizePolicy>
#include <QList>

#include <cmath>
#include <stdio.h>
#include <iostream>

#include "scopeWin.h"
#include "version.h"

scopeWin::scopeWin(deviceHandler *dH, fftEngine *fE, QWidget *parent): QWidget(parent), devHandle(dH),
    waitLabelTimer(new nextSampleWaitTimer), oszi(new oszilloskop(dH, waitLabelTimer)), fftE(fE){

    this->setWindowTitle(QString("FreeOszi - ").append(VERSION));

    oszi->actionToPerform=oszilloskop::getCalDataFromStore; // load calibration data if availble
    oszi->start();

// --- ---  --------- graph window ------------------
    this->initView();

// - ----------------  Top Line --------------------------

    QHBoxLayout *topValuesLabelLayout = new QHBoxLayout(); topValuesLabelLayout->setObjectName(QString("topValuesLabelLayout"));
    //topValuesLabelLayout->set<(QRect(260, 130, 411, 80));
    //topValuesLabelLayout->setContentsMargins(0,0,0,0);
    QLabel *tMCh1 = new QLabel("<h3>Ch1:    Mean</3>", this); tMCh1->setScaledContents(true); tMCh1->setMargin(0);
    tMCh1->setFrameStyle(QFrame::Panel | QFrame::Raised);
    QLabel *tMCh2 = new QLabel("<h3>Ch2:    Mean</h3>", this);
    QLabel *tRCh1 = new QLabel("<h3>RMS</h3>", this);
    QLabel *tRCh2 = new QLabel("<h3>RMS</h3>", this);

    QLabel *vCh1 = new QLabel("<h3>0</h3>", this);
    QLabel *vCh2 = new QLabel("<h3>0</h3>", this);
    QLabel *RCh1 = new QLabel("<h3>0</h3>", this);
    QLabel *RCh2 = new QLabel("<h3>0</h3>", this);

    topValuesLabelLayout->addWidget(tMCh1); topValuesLabelLayout->addWidget(vCh1);
    topValuesLabelLayout->addWidget(tRCh1); topValuesLabelLayout->addWidget(RCh1);

    topValuesLabelLayout->addWidget(tMCh2); topValuesLabelLayout->addWidget(vCh2);
    topValuesLabelLayout->addWidget(tRCh2); topValuesLabelLayout->addWidget(RCh2);
    QRadioButton *scopeViewRB = new QRadioButton("Scope", this);
    QRadioButton *specViewRB = new QRadioButton("Spec", this);
    scopeViewRB->setChecked(true);
    topValuesLabelLayout->addWidget(scopeViewRB);
    topValuesLabelLayout->addWidget(specViewRB);
    QButtonGroup *viewMode = new QButtonGroup;
    viewMode->addButton(scopeViewRB, 1);
    viewMode->addButton(specViewRB, 2);
    connect(viewMode, SIGNAL(buttonClicked(int)), this, SLOT(selectView(int))); // Ch1 coupling selector to slot



    // ------------------- Dials Line ---------------------------------

    QHBoxLayout *dialsLineLayout = new QHBoxLayout(); dialsLineLayout->setObjectName(QString("dialsLineLayout"));
    dialsLineLayout->addLayout(guiCh1Knobs());
    dialsLineLayout->addLayout(guiCh2Knobs());
    dialsLineLayout->addLayout(guiTriggerKnobs());
    dialsLineLayout->addLayout(guiTimebaseKnobs());

    // ------------------- Buttons LINE -----------------------------
    QPushButton *simu= new QPushButton("Simulate", this);
    QPushButton *start= new QPushButton("Start", this);
    QPushButton *single= new QPushButton("Single", this);
    QPushButton *stop= new QPushButton("Stop", this);
                 cal= new QPushButton("Calibrate", this);
    QPushButton *initdev= new QPushButton("Init Device", this);

    QHBoxLayout *buttonsLineLayout = new QHBoxLayout(); buttonsLineLayout->setObjectName(QString("buttonsLineLayout"));
    buttonsLineLayout->addWidget(simu);
    buttonsLineLayout->addWidget(start);
    buttonsLineLayout->addWidget(single);
    buttonsLineLayout->addWidget(stop);
    buttonsLineLayout->addWidget(cal);
    buttonsLineLayout->addWidget(initdev);

    // connects for buttons
    connect(simu, &QPushButton::clicked, this, &scopeWin::rcvStartSimulating);          //start cont simulation
    connect(start, &QPushButton::clicked, this, &scopeWin::rcvStartRunning);     //start cont run
    connect(single, &QPushButton::clicked, this, &scopeWin::rcvRunOnce);         //run once
    connect(stop, &QPushButton::clicked, this, &scopeWin::rcvStopRunning);       //stop cont run
    connect(cal, &QPushButton::clicked, this, &scopeWin::rcvCalibrate);    //calibrate
    connect(initdev, &QPushButton::clicked, this, &scopeWin::initDevice);        //initialise device


// ------------------ MAIN LAYOUT -----------------
    mainLayout = new QVBoxLayout(this); mainLayout->setObjectName(QString("mainLayout"));
    mainLayout->addLayout(topValuesLabelLayout);
    mainLayout->addWidget(scV->getChartView());
    mainLayout->addLayout(dialsLineLayout);
    mainLayout->addLayout(buttonsLineLayout);
    mainLayout->setContentsMargins(0,0,0,0);
    this->setLayout(mainLayout);

    // connect(oszi, SIGNAL(calibrationDone(int)), this, SLOT(rcvCalibrationDone(int))); // set Knobs after calibration
    connect(oszi, &oszilloskop::calibrationDone, this, &scopeWin::rcvCalibrationDone); // set Knobs after calibration
    connect(this, SIGNAL(updateMeanCh1(double)), vCh1, SLOT(setNum(double)));   //update mean of Ch1
    connect(this, SIGNAL(updateMeanCh2(double)), vCh2, SLOT(setNum(double)));   //update mean of Ch2
    connect(this, SIGNAL(updateRMSCh1(double)), RCh1, SLOT(setNum(double)));    //update RMS of Ch1
    connect(this, SIGNAL(updateRMSCh2(double)), RCh2, SLOT(setNum(double)));    //update RMS of Ch2
}

scopeWin::~scopeWin()
{

    devHandle->runGranted=false;           //tell any other thread to stop
    devHandle->wait();                      // another thread can still be running when quitting. wait for it to stop
}

QVBoxLayout * scopeWin::guiCh1Knobs(){
    // - ----------------  CH 1 ----------------------------
    // - ----------------- sensitivity ---------------------
    dialCh1 = new QDial(this);
    dialCh1->setObjectName(QString::fromUtf8("dialCh1"));
    dialCh1->setGeometry(QRect(220, 140, 50, 64));
    dialCh1->setMaximum(5);
    dialCh1->setValue(oszi->sensivCh1_dialVal);
    dialCh1->setWrapping(false);
    dialCh1->setSingleStep(1);
    dialCh1->setPageStep(1);

    QLabel *labelsCH1[3];
    labelsCH1[0]= new QLabel("Ch 1", this); labelsCH1[1]= new QLabel("0.3",this); labelsCH1[2]= new QLabel("V/Div",this);
    QSpacerItem *hzSpacerCh1[2];
    hzSpacerCh1[0]= new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);
    hzSpacerCh1[1]= new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);
    //connect(dialCh1, SIGNAL(valueChanged(int)), this, SLOT(rcvDialValCh1(int)));        // Ch1 dial to translating slot
    connect(dialCh1, &QDial::valueChanged, this, &scopeWin::rcvDialValCh1);        // Ch1 dial to translating slot

    connect(oszi, SIGNAL(sensitivityCh1Changed(qreal)), *(labelsCH1+1), SLOT(setNum(qreal)));   // translating slot to Amplitude label

    // - ----------------- coupling ---------------------
    QRadioButton *couplingCh1[4];
    couplingCh1[0]= new QRadioButton("DC", this);
    couplingCh1[1]= new QRadioButton("AC", this);
    couplingCh1[2]= new QRadioButton("GND", this);
    couplingCh1[3]= new QRadioButton("OFF", this);
    (*couplingCh1)->setChecked(true);               // make to be same as oszi->tbText

    QButtonGroup *couplingSelectGroupCh1 = new QButtonGroup;
    couplingSelectGroupCh1->addButton(*couplingCh1, 1);
    couplingSelectGroupCh1->addButton(*(couplingCh1+1), 2);
    couplingSelectGroupCh1->addButton(*(couplingCh1+2), 3);
    couplingSelectGroupCh1->addButton(*(couplingCh1+3), 4);
    connect(couplingSelectGroupCh1, SIGNAL(buttonClicked(int)), this, SLOT(couplingSelectCh1(int))); // coupling button group Ch1 to slot

    // - ----------------- offset ---------------------
    offsSbCh1 = new QSpinBox(this);
    offsSbCh1->setRange(-130,130);
    offsSbCh1->setValue(oszi->offsCh1_dialVal);
    connect(offsSbCh1, SIGNAL(valueChanged(int)), this, SLOT(rcvDialOffsCh1(int)));                 // offset spin box to slot


    // - ----------------- layouts ---------------------
    QHBoxLayout *ch1LabelLayout;
    ch1LabelLayout = new QHBoxLayout();
    ch1LabelLayout->addItem(hzSpacerCh1[0]);
    ch1LabelLayout->addWidget(labelsCH1[0]);    ch1LabelLayout->addWidget(labelsCH1[1]);    ch1LabelLayout->addWidget(labelsCH1[2]);
    ch1LabelLayout->addItem(hzSpacerCh1[1]);

    QVBoxLayout *ch1CouplingLayout;
    ch1CouplingLayout = new QVBoxLayout();
    ch1CouplingLayout->addWidget(*couplingCh1);
    ch1CouplingLayout->addWidget(*(couplingCh1+1));
    ch1CouplingLayout->addWidget(*(couplingCh1+2));
    ch1CouplingLayout->addWidget(*(couplingCh1+3));


    QHBoxLayout *ch1KnobLayout = new QHBoxLayout();
    ch1KnobLayout->addWidget(dialCh1);
    ch1KnobLayout->addWidget(offsSbCh1);
    ch1KnobLayout->addLayout(ch1CouplingLayout);

    QVBoxLayout *ch1Layout;
    ch1Layout = new QVBoxLayout();
    ch1Layout->addLayout(ch1LabelLayout);    ch1Layout->addLayout(ch1KnobLayout);

    return ch1Layout;
}

QVBoxLayout * scopeWin::guiCh2Knobs(){
    // - ----------------  CH 2 ----------------------------
    // - ----------------- sensitivity ---------------------
    dialCh2 = new QDial(this);
    dialCh2->setObjectName(QString::fromUtf8("dialCh2"));
    dialCh2->setGeometry(QRect(220, 140, 50, 64));
    dialCh2->setMaximum(5);
    dialCh2->setValue(oszi->sensivCh2_dialVal);
    dialCh2->setSingleStep(1);
    dialCh2->setPageStep(1);
    dialCh2->setWrapping(false);
    QLabel *labelsCH2[3];
    labelsCH2[0]= new QLabel("Ch 2", this); labelsCH2[1]= new QLabel("0.3",this);  labelsCH2[2]= new QLabel("V/Div",this);

    QSpacerItem *hzSpacerCh2[2];
    hzSpacerCh2[0]= new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);
    hzSpacerCh2[1]= new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

    connect(dialCh2, SIGNAL(valueChanged(int)), this, SLOT(rcvDialValCh2(int)));                // sensitivity dial Ch2 to slot
    connect(oszi, SIGNAL(sensitivityCh2Changed(qreal)), *(labelsCH2+1), SLOT(setNum(qreal)));   // oszi signal (separate thread) to sensitivity label

    // - ----------------- coupling ---------------------
    QRadioButton *couplingCh2[4];
    couplingCh2[0]= new QRadioButton("DC", this);
    couplingCh2[1]= new QRadioButton("AC", this);
    couplingCh2[2]= new QRadioButton("GND", this);
    couplingCh2[3]= new QRadioButton("OFF", this);
    (*couplingCh2)->setChecked(true);

    QButtonGroup *couplingSelectGroupCh2 = new QButtonGroup;
    couplingSelectGroupCh2->addButton(*couplingCh2, 1);
    couplingSelectGroupCh2->addButton(*(couplingCh2+1), 2);
    couplingSelectGroupCh2->addButton(*(couplingCh2+2), 3);
    couplingSelectGroupCh2->addButton(*(couplingCh2+3), 4);
    connect(couplingSelectGroupCh2, SIGNAL(buttonClicked(int)), this, SLOT(couplingSelectCh2(int))); // coupling button group Ch2 to slot


    // - ----------------- offset ---------------------
    offsSbCh2 = new QSpinBox(this);
    offsSbCh2->setRange(-130,130);
    offsSbCh2->setValue(oszi->offsCh2_dialVal);
    connect(offsSbCh2, SIGNAL(valueChanged(int)), this, SLOT(rcvDialOffsCh2(int)));                 // offset spin box to slot


    // - ----------------- layouts ---------------------
    QHBoxLayout *ch2LabelLayout = new QHBoxLayout();
    ch2LabelLayout->addItem(hzSpacerCh2[0]);
    ch2LabelLayout->addWidget(labelsCH2[0]);    ch2LabelLayout->addWidget(labelsCH2[1]);    ch2LabelLayout->addWidget(labelsCH2[2]);
    ch2LabelLayout->addItem(hzSpacerCh2[1]);

    QVBoxLayout *ch2CouplingLayout;
    ch2CouplingLayout = new QVBoxLayout();
    ch2CouplingLayout->addWidget(*couplingCh2);
    ch2CouplingLayout->addWidget(*(couplingCh2+1));
    ch2CouplingLayout->addWidget(*(couplingCh2+2));
    ch2CouplingLayout->addWidget(*(couplingCh2+3));

    QHBoxLayout *ch2KnobLayout = new QHBoxLayout();
    ch2KnobLayout->addWidget(dialCh2);
    ch2KnobLayout->addWidget(offsSbCh2);
    ch2KnobLayout->addLayout(ch2CouplingLayout);

    QVBoxLayout *ch2Layout = new QVBoxLayout();
    ch2Layout->addLayout(ch2LabelLayout);    ch2Layout->addLayout(ch2KnobLayout);

    return ch2Layout;
}

QVBoxLayout * scopeWin::guiTimebaseKnobs(){
    // - ----------------  Time Base --------------------------
    dialTimeBase = new QDial(this);
    dialTimeBase->setObjectName(QString::fromUtf8("dialTimeBase"));
    dialTimeBase->setGeometry(QRect(220, 140, 50, 64));
    dialTimeBase->setRange(0,15);
    dialTimeBase->setValue(oszi->timeBase_dialVal);
    dialTimeBase->setSingleStep(1);
    dialTimeBase->setPageStep(1);
    QLabel *labelsTimeBase[2];
    labelsTimeBase[0]= new QLabel("Sampling Rate", this); labelsTimeBase[1]= new QLabel(oszi->tbText,this);
    QSpacerItem *hzSpacerTimeBase[2];
    hzSpacerTimeBase[0]= new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);
    hzSpacerTimeBase[1]= new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);
    QLabel *waitLongForSample = new QLabel("", this);
    waitLabelTimer->waitLongForSampleLablel=waitLongForSample;
    connect(oszi, &oszilloskop::waitLabelTimerOnOff, waitLabelTimer, &nextSampleWaitTimer::waitLabelTimerStartStop);

    QHBoxLayout *timeBaseLabelLayout = new QHBoxLayout();
    timeBaseLabelLayout->addItem(hzSpacerTimeBase[0]);
    timeBaseLabelLayout->addWidget(labelsTimeBase[0]);    timeBaseLabelLayout->addWidget(labelsTimeBase[1]);
    timeBaseLabelLayout->addItem(hzSpacerTimeBase[1]);

    QVBoxLayout *timeBaseLayout = new QVBoxLayout();
    timeBaseLayout->addLayout(timeBaseLabelLayout);
    timeBaseLayout->addWidget(waitLongForSample);
    timeBaseLayout->addWidget(dialTimeBase);

    // connects for dials for Time base
    connect(dialTimeBase, SIGNAL(valueChanged(int)), this, SLOT(rcvDialTimeBase(int)));             //Time Base Knob to translating slot
    connect(oszi, SIGNAL(timeBaseKnobTextChanged(QString)), *(labelsTimeBase+1), SLOT(setText(QString)));   // translating slot to Amplitude label

    return timeBaseLayout;
}

QVBoxLayout * scopeWin::guiTriggerKnobs(){
    // - ----------------  Trigger ----------------------------
    dialTrigLev = new QDial(this);                              // trigger level dial
    dialTrigLev->setObjectName(QString::fromUtf8("dialTrigLev"));
    dialTrigLev->setGeometry(QRect(220, 140, 50, 64));
    dialTrigLev->setRange(0,255);
    dialTrigLev->setValue(oszi->trigLevel_dialVal);
    QLabel *labelsTrigLev[3];
    labelsTrigLev[0]= new QLabel("Trigger Level", this); labelsTrigLev[1]= new QLabel("0.01",this); labelsTrigLev[2]= new QLabel("V",this);
    labelsTrigLev[1]->setFixedWidth(46);
    labelsTrigLev[1]->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    QSpacerItem *hzSpacerTrigLev[2];
    hzSpacerTrigLev[0]= new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);
    hzSpacerTrigLev[1]= new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);
    connect(dialTrigLev, SIGNAL(valueChanged(int)), this, SLOT(rcvDialTriggerLevel(int)));          // Time Base Knob to translating slot
    connect(oszi, SIGNAL(triggerLevelChanged(qreal)), *(labelsTrigLev+1), SLOT(setNum(qreal)));     // calculating thread to label


    QCheckBox *trigEnabled = new QCheckBox("Enabled", this);    // trigger enabled/disabled
    trigEnabled->setChecked((oszi->trigEnabled_dialVal == Qt::Checked)? true: false); // set same as oszi->trigEnabled_dialVal
    connect(trigEnabled, SIGNAL(stateChanged(int)), this, SLOT(triggerEnabled(int)));

    QRadioButton *trigChanSel[2];                               // trigger channel selector
    trigChanSel[0]= new QRadioButton("Ch1", this);
    trigChanSel[1]= new QRadioButton("Ch2", this);
    QButtonGroup *trigChanSelectGroup = new QButtonGroup;
    trigChanSelectGroup->addButton(*trigChanSel, 1);
    trigChanSelectGroup->addButton(*(trigChanSel+1), 2);
    (*trigChanSel)->setChecked(true);                           // set same as oszi->trigChanSel_dialVal
    connect(trigChanSelectGroup, SIGNAL(buttonClicked(int)), this, SLOT(triggerChanSelect(int)));


    QRadioButton *trigEdgeSel[2];                               //trigger edge selector
    trigEdgeSel[0]= new QRadioButton("rE", this);
    trigEdgeSel[1]= new QRadioButton("fE", this);
    QButtonGroup *trigEdgeSelectGroup = new QButtonGroup;
    trigEdgeSelectGroup->addButton(*trigEdgeSel, 1);
    trigEdgeSelectGroup->addButton(*(trigEdgeSel+1), 2);
    (*trigEdgeSel)->setChecked(true);                           // set same as oszi->trigEdge_dialVal
    connect(trigEdgeSelectGroup, SIGNAL(buttonClicked(int)), this, SLOT(triggerEdgeSelect(int)));


    QHBoxLayout *trigLabelLayout = new QHBoxLayout();
    trigLabelLayout->addItem(hzSpacerTrigLev[0]);
    trigLabelLayout->addWidget(labelsTrigLev[0]);    trigLabelLayout->addWidget(labelsTrigLev[1]);    trigLabelLayout->addWidget(labelsTrigLev[2]);
    trigLabelLayout->addItem(hzSpacerTrigLev[1]);

    QVBoxLayout *trigSelectorLayout = new QVBoxLayout();
    trigSelectorLayout->addWidget(*trigChanSel);
    trigSelectorLayout->addWidget(*(trigChanSel+1));
    trigSelectorLayout->addWidget(*trigEdgeSel);
    trigSelectorLayout->addWidget(*(trigEdgeSel+1));

    QHBoxLayout *trigKnobLayout = new QHBoxLayout();
    trigKnobLayout->addWidget(dialTrigLev); trigKnobLayout->addLayout(trigSelectorLayout);

    QVBoxLayout *trigLayout = new QVBoxLayout();
    trigLayout->addLayout(trigLabelLayout);
    trigLayout->addWidget(trigEnabled);
    trigLayout->addLayout(trigKnobLayout);

    return trigLayout;
}

void scopeWin::initView(){
    scV = new scopeView(oszi,  devHandle);
    connect(devHandle, &deviceHandler::sampleDataReady, scV, &scopeView::updateSeries);     // new sample sits in buffer
    connect(scV, &scopeView::updateMeanRMS, this,  &scopeWin::findMeanRMSTwoChan);
    connect(oszi, SIGNAL(sensitivityCh1Changed(qreal)), scV, SLOT(setYRangeCh1(qreal)));
    connect(oszi, SIGNAL(sensitivityCh2Changed(qreal)), scV, SLOT(setYRangeCh2(qreal)));
    connect(oszi, SIGNAL(visibilityCh1Changed(bool)), scV, SLOT(setVisibleCh1(bool)));
    connect(oszi, SIGNAL(visibilityCh2Changed(bool)), scV, SLOT(setVisibleCh2(bool)));
    connect(oszi, SIGNAL(triggerStateChanged(int)), scV, SLOT(setTriggerState(int)));
    connect(oszi, SIGNAL(triggerLevelChanged(qreal)), scV, SLOT(setTriggerLevel(qreal)));
    connect(oszi, SIGNAL(triggerChannelChanged(int)), scV, SLOT(setTriggerChannel(int)));
    connect(oszi, SIGNAL(timeBaseChanged(qreal, qreal, QString)), scV, SLOT(setTimeBase(qreal, qreal, QString)));   // translating slot to Amplitude label
    actualViewObject = scV;
}

void scopeWin::selectView(int val){         //slot for selecting view (1: scopeView or 2: specView)
    switch(val){
    case 1:{                                // set scopeView
        if (scV != nullptr) { break;}                                                       // if scV exists then scopeView is already active -> nothing to do
        scV = new scopeView(oszi,  devHandle);                                              // else create a new scopeView
        mainLayout->replaceWidget(fcV->getChartView(), scV->getChartView());                // make newly created scopeView visible
        actualViewObject = scV;
        delete fcV; fcV=nullptr;                                                            //delete previously displayed scopevView (not needed any more)
        connect(devHandle, &deviceHandler::sampleDataReady, scV, &scopeView::updateSeries); // connect signals; disconnect happens automatically when scV gets deleted, see QObject::disconnect()
        connect(scV, &scopeView::updateMeanRMS, this,  &scopeWin::findMeanRMSTwoChan);
        connect(oszi, &oszilloskop::sensitivityCh1Changed, scV, &scopeView::setYRangeCh1);
        connect(oszi, &oszilloskop::sensitivityCh2Changed, scV, &scopeView::setYRangeCh2);
        connect(oszi, SIGNAL(visibilityCh1Changed(bool)), scV, SLOT(setVisibleCh1(bool)));
        connect(oszi, SIGNAL(visibilityCh2Changed(bool)), scV, SLOT(setVisibleCh2(bool)));
        connect(oszi, &oszilloskop::triggerStateChanged,   scV, &scopeView::setTriggerState);
        connect(oszi, &oszilloskop::triggerLevelChanged,   scV, &scopeView::setTriggerLevel);
        connect(oszi, &oszilloskop::triggerChannelChanged, scV, &scopeView::setTriggerChannel);
        connect(oszi, &oszilloskop::timeBaseChanged,       scV, &scopeView::setTimeBase);   // translating slot to Amplitude label
        break;
        }
    case 2:{                                // set specView
        if (fcV != nullptr) { break;}                                                       //if fcV exists then it is active -> nothing to do
        fcV = new specView(oszi, &(oszi->sensitivityCh1), &(oszi->sensitivityCh2), devHandle, fftE, oszi->fs);  //else create a new specView
        mainLayout->replaceWidget(scV->getChartView(), fcV->getChartView());                // make newly created specView visible
        actualViewObject = fcV;
        delete scV; scV=nullptr;                                                            //delete previously displayed scopevView (not needed any more)
        connect(devHandle, &deviceHandler::sampleDataReady, fcV, &specView::updateSeries);  // connect signals; disconnect happens automatically when scV gets deleted, see QObject::disconnect()
        connect(fcV, &specView::updateMeanRMS, this,  &scopeWin::findMeanRMSTwoChan);
        connect(oszi, SIGNAL(sensitivityCh1Changed(qreal)), fcV, SLOT(updateYRangeCh1(qreal)));
        connect(oszi, SIGNAL(sensitivityCh2Changed(qreal)), fcV, SLOT(updateYRangeCh2(qreal)));
        connect(oszi, SIGNAL(visibilityCh1Changed(bool)), fcV, SLOT(setVisibleCh1(bool)));
        connect(oszi, SIGNAL(visibilityCh2Changed(bool)), fcV, SLOT(setVisibleCh2(bool)));
        break;
        }
    }
}

void scopeWin::initDevice(){  // initaize usb port and connection to device
    oszi->actionToPerform=oszilloskop::initialize;
    oszi->start();
}

void scopeWin::rcvStartRunning(){ // run continuously
    oszi->actionToPerform=oszilloskop::startRunning;
    oszi->start();
}

void scopeWin::rcvStartSimulating(){ // simulate oszilloscope sample data coming in
    oszi->actionToPerform=oszilloskop::startSimulating;
    oszi->start();
}

void scopeWin::rcvRunOnce(){ // run once
    oszi->actionToPerform=oszilloskop::runOnce;
    oszi->start();
}

void scopeWin::rcvStopRunning(){ // stop continuous run/simulate
        devHandle->runGranted=false;                             // revoke permission to run
}

void scopeWin::rcvDialValCh1(int dialVal){  // recv level dial setting from knob for ch1
    oszi->sensivCh1_dialVal=dialVal;
    oszi->actionToPerform=oszilloskop::setSensitivityCh1;
    oszi->start();
}
void scopeWin::rcvDialValCh2(int dialVal){  // recv level dial setting from knob for ch2
    oszi->sensivCh2_dialVal=dialVal;
    oszi->actionToPerform=oszilloskop::setSensitivityCh2;
    oszi->start();
}

void scopeWin::rcvDialOffsCh1(int dialVal){     // recv spin box setting for offs for Ch1
    oszi->offsCh1_dialVal=dialVal;
    oszi->actionToPerform=oszilloskop::setOffsetCh1;
    oszi->start();
}
void scopeWin::rcvDialOffsCh2(int dialVal){     // recv spin box setting for offs for Ch2
    oszi->offsCh2_dialVal=dialVal;
    oszi->actionToPerform=oszilloskop::setOffsetCh2;
    oszi->start();
}

void scopeWin::couplingSelectCh1(int dialVal){   // recv coupling setting for Ch1
     oszi->couplingCh1_dialVal = dialVal;
     oszi->actionToPerform=oszilloskop::setCouplingCh1;
     oszi->start();
}
void scopeWin::couplingSelectCh2(int dialVal){  //recv coupling setting for Ch2
    oszi->couplingCh2_dialVal = dialVal;
    oszi->actionToPerform=oszilloskop::setCouplingCh2;
    oszi->start();
}

void scopeWin::rcvDialTimeBase(int dialVal){            // recv time base setting
    // std::cout << "scopeWin::rcvDialTimeBase(int dialVal):" << dialVal << std::endl;
    oszi->timeBase_dialVal=dialVal;
    oszi->actionToPerform=oszilloskop::setTimeBase;
    oszi->start();
}

void scopeWin::rcvDialTriggerLevel(int dialVal){        // recv trigger level setting
    oszi->trigLevel_dialVal=dialVal;
    oszi->actionToPerform=oszilloskop::setTriggerLevel;
    oszi->start();
}
void scopeWin::triggerChanSelect(int dialVal){          // recv trigger channel selection
    oszi->trigChanSel_dialVal=dialVal;
    oszi->actionToPerform=oszilloskop::setTriggerChannel;
    oszi->start();
}
void scopeWin::triggerEdgeSelect(int dialVal){          // recv trigger edge selecion
    oszi->trigEdge_dialVal=dialVal;
    oszi->actionToPerform=oszilloskop::setTriggerEdge;
    oszi->start();
}
void scopeWin::triggerEnabled(int dialVal){             // rect trigger enabled/disabled
    oszi->trigEnabled_dialVal=dialVal;
    oszi->actionToPerform=oszilloskop::setTriggerEnable;
    oszi->start();
}

void scopeWin::rcvCalibrate(){ // run calibration
    //printf("scopeWin::rcvCalibrate() threadId: %lu\n", pthread_self()); fflush(stdout);
    cal->setText("Calibrating...");
    oszi->actionToPerform=oszilloskop::calibrate;
    oszi->start();
}

void scopeWin::rcvCalibrationDone(int result){
    //printf("scopeWin::rcvCalibration(), result: %d\n", result); fflush(stdout);
    switch(result){
    case oszilloskop::cal_success:{
        // --- setKnobs and dials where they were... ---------------------
        dialCh1->setValue(2);dialCh1->setValue(3);
        dialCh2->setValue(2);dialCh2->setValue(3);
        dialTimeBase->setValue(9);dialTimeBase->setValue(10);
        dialTrigLev->setValue(150);dialTrigLev->setValue(160);
        cal->setText("Calibration done");

        // --- store calibration data -------------------------------------
        //printf("oszilloskop::sl_calibrationDone()...\n"); fflush(stdout);
        oszi->actionToPerform=oszilloskop::storeCalData;
        oszi->start();
        break;
    }
    case oszilloskop::cal_deviceBusy:{
        cal->setText("Calibration");
        break;
    }
    case oszilloskop::cal_fail:{
        cal->setText("Calibration failed");
        break;
    }
    }
}
void scopeWin::rcvCalibrationRestored(){
    cal->setText("Calibration restored");
}

void scopeWin::findMeanRMSTwoChan( unsigned char *buf, int len){
    double M_RMS[4];
    double *meanCh1=M_RMS, *meanCh2=M_RMS+1, *RMSCh1=M_RMS+2, *RMSCh2=M_RMS+3;
    *meanCh1=0, *meanCh2=0;
    for(int l=0; l<len; l++){
        *meanCh1=*meanCh1 + 4*oszi->sensitivityCh1 *(((double) *(buf+ 2*l  ) )/127.5-1);
        *meanCh2=*meanCh2 + 4*oszi->sensitivityCh2 *(((double) *(buf+ 2*l+1) )/127.5-1);
    }
    *meanCh1=*meanCh1/len;
    *meanCh2=*meanCh2/len;
    //printf("findMean(): meanCh1: %f\n", *meanCh1);

    *RMSCh1=0, *RMSCh2=0;
    for(int l=0; l<len; l++){
        *RMSCh1=*RMSCh1 + pow( 4*oszi->sensitivityCh1 * (((double) *(buf+ 2*l  ) )/127.5-1), 2.0 );
        *RMSCh2=*RMSCh2 + pow( 4*oszi->sensitivityCh2 * (((double) *(buf+ 2*l+1) )/127.5-1), 2.0 );
    }
    //printf("fMTC: %d\n", *(buf+8));
    *RMSCh1=sqrt(*RMSCh1/len);
    *RMSCh2=sqrt(*RMSCh2/len);
    //printf("findRMS(): RMSCh1: %f\n", *RMSCh1);
    emit updateMeanCh1(*meanCh1); emit updateMeanCh2(*meanCh2);
    emit updateRMSCh1(*RMSCh1); emit updateRMSCh2(*RMSCh2);
}

void scopeWin::closeEvent(QCloseEvent *event) {
    printf("close event... \n"); fflush(stdout);
    if (otherWin != nullptr){otherWin->close();}
     QCoreApplication::quit();
     waitLabelTimer->stopTimer();
}
