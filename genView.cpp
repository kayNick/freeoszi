#include "genView.h"

genView::genView()
{
    m_chart = new QChart;
    tView = new QChartView(m_chart);
    tView->setMinimumSize(5*60, 5*45);
    tView->setContentsMargins(0,0,0,0);
    m_chart->setWindowFrameMargins(0,0,0,0);
    m_chart->setContentsMargins(0,0,0,0);

    sigSeries = new QLineSeries;
    m_chart->addSeries(sigSeries);


    sigAxis = new QValueAxis;
    sigAxis->setRange(0, 255);
    sigAxis->setLabelFormat(" ");

    timeAxis = new QValueAxis;
    timeAxis->setRange(0, 511);
    timeAxis->setLabelFormat(" ");

    m_chart->addAxis(timeAxis, Qt::AlignBottom);
    sigSeries->attachAxis(timeAxis);

    m_chart->addAxis(sigAxis, Qt::AlignLeft);
    sigSeries->attachAxis(sigAxis);

    m_chart->legend()->hide();

    sigVector = new QVector<QPointF>;
}

QChartView * genView::getChartView(){
    return tView;
}

void genView::initSeries(unsigned char *sigData, int length){
    // printf("genView::initSeries()\n"); fflush(stdout);

    for (int l=0; l < length; l++){
        sigVector->append(QPointF( (qreal)l,  (qreal) *(sigData+l)  )  );
    }
    sigSeries->replace(*sigVector);         // updates diyplayed signal form
}

void genView::updateSeries(unsigned char *sigData, int length){
    // printf("genView::updateSeries()\n"); fflush(stdout);
    for (int l=0; l < length; l++){
        sigVector->replace(l, QPointF( (qreal)l,  (qreal) * (sigData+l) ));
    }
    sigSeries->replace(*sigVector);         // updates diyplayed signal form
}
