#include "fftEngine.h"

fftEngine::fftEngine()
{
    //------------- for fftw operation ---------------

    inCh1 =(fftw_complex *)fftw_malloc(sizeof(fftw_complex) * 4096);
    outCh1=(fftw_complex *)fftw_malloc(sizeof(fftw_complex) * 4096);
    inCh2 =(fftw_complex *)fftw_malloc(sizeof(fftw_complex) * 4096);
    outCh2=(fftw_complex *)fftw_malloc(sizeof(fftw_complex) * 4096);

    planCh1 = fftw_plan_dft_1d(4096, inCh1, outCh1, FFTW_FORWARD, FFTW_MEASURE);
    planCh2 = fftw_plan_dft_1d(4096, inCh2, outCh2, FFTW_FORWARD, FFTW_MEASURE);


}
fftEngine::~fftEngine(){
    fftw_destroy_plan(planCh1); fftw_destroy_plan(planCh2);
    fftw_free(inCh1); fftw_free(inCh2); fftw_free(outCh1); fftw_free(outCh2);
    void fftw_cleanup(void);
}
void fftEngine::exec(){
    fftw_execute(planCh1); fftw_execute(planCh2);

}
