<< Version 0.1.35 is here. Storing and restoring calibration data should work now. >>

# Velleman PCSGU250 usb oscilloscope/generator

Hello,

this project aims at creating a free and open source software for the popular Velleman PCSGU250 usb oscilloscope/generator. It is organized as a simple QT-Creator project. Just start QT-Creator and open this project, build and run it.

You can build it on command line as well:

+ cd into project directory

+ qmake FreeOszi.pro

+ make


**View of the Oszilloscope screen (time domain)**  
![Scope view](https://gitlab.com/kayNick/freeoszi/-/raw/master/public/scope.png)

**View of the Frequency Analyser screen (frequency domain)**  
![Scope view](https://gitlab.com/kayNick/freeoszi/-/raw/master/public/spectrum.png)

**View of the Generator screen**   
![Scope view](https://gitlab.com/kayNick/freeoszi/-/raw/master/public/generator.png)

## Dependencies

To compile it you will need:

* qt5

* qt5-charts

* libusb

* fftw3

## Installing Dependencies via apt
`sudo apt install qtcreator libqt5charts5-dev libusb-1.0-0-dev libfftw3-dev`

## udev Rules

Copy 99-sampler.rules to /etc/udev/rules.d/ to give user level access to the USB device

`cp 99-sampler.rules /etc/udev/rules.d/`

## Additional Comments

For now, both oscilloscope channels work, as well as most of the typical controls like level, sampling rate, trigger level/source/edge..., coupling, a working and sane spectrum display (with a frequency axis(!)). The generator part works as well although there are some (hopefully minor) kinks to work out still.

Good luck, have fun.

Comments, feedback and support is appreciated.

## note - power delivery to the scope. 
The scope is not capable of asking USB2/3 ports of increased power delivery. So it is stuck with the 100 mA a USB port delivers on itself. This seems 
to be sufficient for the scope on its own. The internal rf - generator, (its output amp to be precise) needs +-15 V to operate correctly. These higher voltages are generated internally by means of a dc-dc converter.

And this conversion requires quite a bit of power, which is drawn from
the usb port. This power draw leads to a voltage drop on the vdd pin of
the usb line. This voltage drop in turn causes the internal controller
to crash/reset as it has no internal means to stabilize it. Using poor
quality usb cables (very thin conductors with high resistance) worsens
this problem certainly.

Some suggestions what to do about it:

+  (obvious) use high quality usb cables with good connectors and good
conductors

* raspberry pis are known to have limited power delivery capabilities on
their usb ports. Using power bricks capable of delivering > 3A might
help. (but not necessarily as the power connector on the pi (micro-usb afaik) has significant contact resistance leading again to voltage drop...)

* using a self powered usb-hub (with its own power brick) between your
pi and the scope might help

* trying out operation on a wall-powered desktop pc (using usb ports on
its back to minimize usb line length (resistance) ). I'd love from hear
from you, whether that makes your problem go away.

* i have been successful cutting the oscilloscope's internal connection
between the usb socket and the dc-dc converter and powering it from a
separate 5 V dc source.


Many people complain about similar problems which all seem to come down
to a not (sufficiently) stable  5 V supply voltage for this scope. Thin
conductors and bad poor quality connectors together with limitations in
power delivery capabilities in modern usb hosts seems to be the main
culprit.


Kay


P. S. I keep working on this project doing changes occasionally so make sure to always grab the newest code from gitlab. 
