#ifndef SPECVIEW_H
#define SPECVIEW_H

#include <QChart>
#include <QChartView>
#include <QLineSeries>
#include <QValueAxis>
#include <QPointF>
#include <QtMath>
#include <QTimer>
#include "oszilloskop.h"
#include "deviceHandler.h"
#include "fftEngine.h"

QT_CHARTS_BEGIN_NAMESPACE
class QLineSeries;
class QChart;
QT_CHARTS_END_NAMESPACE
QT_CHARTS_USE_NAMESPACE


class specView : public QObject
{
    Q_OBJECT

public:
    specView(oszilloskop *, qreal *seneitivityCh1, qreal *sensitivityCh2, deviceHandler *dH, fftEngine *fE, int fs);
    ~specView();
    QChartView * getChartView();


public slots:
    void updateSeries();
    void updatefRange(int);
    void updateYRangeCh1(qreal);
    void updateYRangeCh2(qreal);
    void setVisibleCh1(bool);
    void setVisibleCh2(bool);
    void hideOvL();
    void showOvL();
    void updateOnResize(const QRectF &rect);
signals:
    void updateMeanRMS(unsigned char *, int);


private:
    qreal maxCh1, maxCh2;
    qreal df=61.035;
    deviceHandler *devHandle;
    fftEngine *fftE;
    QChart * f_chart;
    QChartView *fView;
    QVector<QPointF> *ch1SQVec, *ch2SQVec;
    QLineSeries *ch1_SpecSeries, *ch2_SpecSeries;
    QValueAxis *axisFreq;
    unsigned char sampleDataBuffer[8192];
    unsigned char tmp[8192]; // for undoing weird sample shift...
    QGraphicsSimpleTextItem *ovL;
    QGraphicsSimpleTextItem *author;


    void calcSpec();

};

#endif // SPECVIEW_H
