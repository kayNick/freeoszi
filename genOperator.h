#pragma once

#include <QThread>
#include <QtMath>
#include "deviceHandler.h"

class genOperator : public QThread{
    Q_OBJECT
public:
    explicit genOperator(deviceHandler *dH);
    ~genOperator();
    void run();         //genOperator thread entry point

    int opToExec=0;
    int freqIndex=0;
    float freq=5000;
    unsigned char amplCoarse, amplFine, amplSoft;
    unsigned char offsVal=0x7f;
    unsigned char byte5=0x23, byte6=0x0a;
    int genSigForm=1;
    //int genSigAmpl;

public slots:

signals:
    void genSigFormIsSet();

private:
    deviceHandler *devHandle;

    void freq2BytePattern();
    void setSigForm();

};
