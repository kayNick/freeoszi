#include <stdio.h>
#include "genOperator.h"


genOperator::genOperator(deviceHandler *dH): devHandle(dH){}
genOperator::~genOperator(){}


void genOperator::run(){                                                                            // gen operater thread entry point...
    float fq=5000000;                                                                               // temp frequecy buffer used to track changes in frequency selector setting
    // printf ("### genOperator::run(), genSigForm: %d\n", genSigForm); fflush(stdout);

    // keep updating genCmdBuf as long as amplVal, offsVal keep changing (from GUI)
    while (fq != freq || *(devHandle->genCmdBuf+4) != amplCoarse || *(devHandle->genCmdBuf+3) != offsVal || *(devHandle->genCmdBuf+5) != amplFine + 0x10*1 || *(devHandle->genCmdBuf+6) != byte6 ){        //this runs in another thread (not main thread), freqIndex can change during devHandle->genCmdSend()...
        freq2BytePattern(); fq=freq;
        *(devHandle->genCmdBuf+3) = offsVal;                                                        // set the actual dc offset setting to generator command buffer
        *(devHandle->genCmdBuf+4) = amplCoarse;                                                     // set the actual amplitude setting to generator command buffer
        *(devHandle->genCmdBuf+5) = amplFine + 0x10*1;                                              // set fine amplitude (least 3 bits) and keep led on (dimmed)
        *(devHandle->genCmdBuf+6) = byte6;
    }
    setSigForm();
    if (devHandle->scopeIsRunning){
        devHandle->genCmdHasChanged=true; }                                                         // if device handler thread is running, just notify it to send changed generator command
    else{
        devHandle->genCmdSend();                                                                    // if device handler thread is not running, execute generator command sending routine dirctly in this thread
    }
}

void genOperator::freq2BytePattern(){  // caluclate special byte pattern form frequency required by device
    unsigned long powsOf2[8];
    unsigned char buf[6];
    float HzPerDigit=2.0*355.27136788e-9;
    unsigned long fScopeUnit= freq/HzPerDigit; // this is in x86 endianess and probably two's complement; we nee to translate that...

    powsOf2[0] =                 1; // 2**(0*8) = 256**0
    powsOf2[1] =               256; // 2**(1*8) = 256**1
    powsOf2[2] =             65536; // 2**(2*8) = 256**2
    powsOf2[3] =          16777216; // 2**(3*8) = 256**3
    powsOf2[4] =        4294967296; // 2**(4*8) = 256**4;
    powsOf2[5] =     1099511627776; // 2**(5*8)
    powsOf2[6] =   281474976710656; // 2**(6*8)
    powsOf2[7] = 72057594037927936; // 2**(7*8)


    // buf[0]= (ul % 256**1)/(256**0);    ul = ul - buf[0]*(256**0);
    // buf[1]= (ul % 256**2)/(256**1);    ul = ul - buf[1]*(256**1);
    // buf[2]= (ul % 256**3)/(256**2);    ul = ul - buf[1]*(256**2);


    for (int k=0; k<6; k++){
        buf[k] = (unsigned char)  ((fScopeUnit % powsOf2[k+1])/powsOf2[k]);    // translate fSU (in x86 endianess two's complement)
        fScopeUnit = fScopeUnit-buf[k]*powsOf2[k];                                    // to LE (little endian, no complement)
        // printf("%2x ", buf[k]);
    }
    // printf("\n"); fflush(stdout);
    memcpy(devHandle->genFreqCmdBuf+11, buf, 6);            // copy 6 byte frequency representaion (little endian, no complement) to devHandle frequency command buffer
}

void genOperator::setSigForm(){                         // fill devHandle->genSampleBuffer with chosen signal form
    // printf ("### genOperator::setSigForm(), genSigForm: %d\n", genSigForm); fflush(stdout);

    switch (genSigForm) {
    case 0:{ // dc
        for (int l=0; l<512 ; l++ ) {
            devHandle->genSampleBuffer[l]=amplSoft;
        }
        break;
    }
    case 1:{ //sin //ampl: 109.5, mean: 125.5
        for (int l=0; l<512 ; l++ ) {
            devHandle->genSampleBuffer[l]=(unsigned char)(128.0 + amplSoft/2.0* qSin((qreal)  2.0 * 3.14 * l /512.0 )) ;
        }
        break;
    }
    case 2:{ //rect
        for (int l=0; l<256 ; l++ ) {
            devHandle->genSampleBuffer[l]=(unsigned char)128.0 + amplSoft/2.0 ;
            devHandle->genSampleBuffer[l+256]=(unsigned char)128.0 - amplSoft/2.0 ;
         }
        break;
    }
    case 3:{ //saw
        for (int l=0; l<512 ; l++ ) {
            devHandle->genSampleBuffer[l]=(unsigned char)(128.0 +amplSoft/255.0*(1.0*(l % 256)-128.0)) ;
        }
        break;
    }
    case 4:{ //sinc

        break;
    }
    }
    emit genSigFormIsSet();
    // printf ("--- genOperator::setSigForm() done.\n"); fflush(stdout);
}
